<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class SalesController extends AppController {
	public $uses = array();
  	public $components = array('Session', 'Email', 'Upload', 'RequestHandler', 'Cookie', 'Paginator');
  	public function isAuthorized($user)
    {
        return true;
    }
  	public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->params['action'] != 'login'){
           if(!$this->Session->check('Admin') ){
           		if($this->params['action'] == 'forgot_password'){
           			$this->admin_forgot_password();
           		}else{
           			$this->redirect(array('action'=>'login'));
           		}	
           	}
       	}
        $this->loadModel('Admin');
        $id = $this->Session->read('Admin.id');
        $profile = $this->Admin->findById($id);
        //echo "<pre>";print_r($profile);die;
        $this->set(compact('profile')); 	

  	}
    public function SalesList(){
      $this->layout="admin";
      $this->loadModel("Admin");
      if(!empty($_GET['keyword']))
      {
        $this->Paginator->settings = array('Admin' => array('limit' => 10, 'conditions' => array('OR' => array(array('email LIKE' => '%'.$_GET['keyword'].'%'), array('username LIKE' => '%'.$_GET['keyword'].'%')),'type'=>'1')));
      }
      else
      $this->Paginator->settings = array('Admin' => array('conditions'=>array('type'=>'1'),'limit' => 10));  
      $user_list=$this->paginate('Admin');
      //pr($user_list);die;
      $this->set(compact('user_list'));
    }

    public function addSalePerson($id = null){
      $this->layout="admin";
      $this->loadModel("Admin");
      if(!empty($id)){
        $data = $this->Admin->findById($id);
        $this->set(compact('data','id'));
      }
      if($this->request->is('post')){
        $data = $this->data;
        if(empty($id)){
          $data['Admin']['created_date'] = date('Y-m-d H:i:s');
          $data['Admin']['password'] = md5($this->data['Admin']['password']);
          $data['Admin']['type'] = 1;
        }
        try{
          $this->Admin->save($data);
          $this->redirect(array('action' => 'SalesList'));
        }catch(Exception $e){
          $this->Session->write('success-msg',$e->getMessage());
          $this->redirect($this->referer());
        }  
      }
    }

    public function delete($id = null){
      $this->loadModel('Admin');
      $this->Admin->delete($id);
      $this->redirect($this->referer());
    }

    public function saleTickectList($id=null)
    {
      $this->layout = "admin";
      $this->loadModel('Order');
      $this->loadModel('User');
      $this->loadModel('Location');
      $this->loadModel('FlightDetail');

      $this->Order->bindModel(array('belongsTo' => array('User' => array('className' => 'User','foreignKey' => 'user_id'))));
      $this->Order->bindModel(array('belongsTo' => array('Flight' => array('className' => 'FlightDetail','foreignKey' => 'flight_id'))));
      $this->Order->bindModel(array('belongsTo' => array('ReturnFlight' => array('className' => 'FlightDetail','foreignKey' => 'Flight_id_return'))));
      $this->Order->bindModel(array('belongsTo' => array('From' => array('className' => 'Location','foreignKey' => 'source'))));
      $this->Order->bindModel(array('belongsTo' => array('To' => array('className' => 'Location','foreignKey' => 'destination'))));
      $this->Paginator->settings = array('Order' => array('limit' => 10,'recursive' => 2,'order' => array('Order.id' => 'DESC')));
      $orders=$this->paginate('Order');
      //$orders = $this->Order->find('all');
      $this->set(compact('orders'));
    }
  }
