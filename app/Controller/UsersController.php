<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class UsersController extends Controller {
	public $uses = array();
  	public $components = array('Session', 'Email', 'Resize', 'Upload', 'RequestHandler', 'Cookie', 'Paginator');
  	public function isAuthorized($user)
    {
        return true;
    }
  	public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->params['action'] != 'login'){
           if(!$this->Session->check('Admin') ){
           		if($this->params['action'] == 'forgot_password'){
           			$this->admin_forgot_password();
           		}else{
           			$this->redirect(array('action'=>'login'));
           		}	
           	}
       	}
        $this->loadModel('Admin');
        $id = $this->Session->read('Admin.id');
        $profile = $this->Admin->findById($id);
        //echo "<pre>";print_r($profile);die;
        $this->set(compact('profile')); 	

  	}

  	public function login(){
      $this->layout="admin";
      $this->loadModel("Admin");
      if($this->request->is('post')) 
      {
        $data = $this->data;
        $username = $data['Admin']['email'];
        $password = md5($data['Admin']['password']);
        $check_login = $this->Admin->find('first', array('conditions' => array('OR'=>array(array('Admin.username'=>$username),array('Admin.email'=>$username)),'Admin.password'=>$password)));
        if(!empty($check_login)){
          $this->Session->write('Admin',$check_login['Admin']);
          $this->redirect(array('action'=>'dashboard'));
        }
      }
  	}

  	public function logout(){
	    $this->Session->destroy();
	    $this->redirect(array('action'=>'login'));
	 }
  	public function dashboard(){
  		$this->layout="admin";
  	}
    public function changepassword(){
      $this->layout="admin";
      $this->loadModel("Admin");
      if($this->request->is('post'))
      {
        $data1 = $this->request->data;
        $data['Admin'] = $data1['Admin'];
        $data['Admin']['password'] = md5($data['Admin']['password']);
        $this->Admin->id=$this->Session->read('Admin.id');
        $this->Admin->save($data);
        $this->Session->write('Admin.password', $data['Admin']['password']);
        $this->Session->write('success-msg','Your password has been changed.');
        $this->redirect(array('action'=>'dashboard'));
      }
    }
    public function check_password(){
      $this->loadModel('User');
      if($this->Session->read('Admin.password') == md5($_POST['oldpass'])){
        echo 'true';
      }
      else{
        echo 'false';
      }
      die;
    }
    public function profile(){
      $this->layout="admin";
      $this->loadModel('Admin');
      $id = $this->Session->read('Admin.id');
      $profile = $this->Admin->findById($id);
      //pr($profile);die;
      $this->set(compact('profile'));
    }
    public function editProfile($id=null){
      $this->layout="admin";
      $this->loadModel('Admin');
      $id = base64_decode($id);
      $profile = $this->Admin->findById($id);
      //pr($profile);die;
      $this->set(compact('profile'));
      if($this->request->is('post'))
      {
        $data = $this->request->data;
        if (!empty($_FILES)) {
                if (is_uploaded_file($_FILES['DishImage']['tmp_name'])) {
                  $image = $this->Components->load('resize');
                  //$image1 = $this->Components->load('resize');  
                  $destination1 = realpath('../webroot/img/profile') . '/';
                  $filename = time().'-'.$_FILES['DishImage']['name'];
                  $image->resize($_FILES['DishImage']['tmp_name'],$destination1.$filename,'aspect_fit',200,200,0,0,0,0);
                  $data['Admin']['image'] = $filename;
                }      
              }
        //echo "<pre>";print_r($data);die;
        $this->Admin->save($data);
        $this->redirect(array('action'=>'profile'));
      }
    }
  	public function user(){
  		$this->layout="admin";
  		$this->loadModel("User");
      if(!empty($_GET['keyword']))
      {
        $this->Paginator->settings = array('User' => array('limit' => 10, 'conditions' => array('OR' => array(array('email LIKE' => '%'.$_GET['keyword'].'%'), array('name LIKE' => '%'.$_GET['keyword'].'%')))));
      }
      else
      $this->Paginator->settings = array('User' => array('limit' => 10));
      $user_list=$this->paginate('User');
      //pr($user_list);die;
  		$this->set(compact('user_list'));
  	}
    public function update_user($id=null){
        $this->loadModel('User');
        $data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
        if($data['User']['varify_status'] == '0'){
            $this->User->id = $id;
            $this->User->savefield('varify_status',1); 
        }else{
            $this->User->id = $id;
            $this->User->savefield('varify_status',0); 
        } 
        
        $this->redirect($this->referer());
    }

    public function contactaddress()
    {
      if($this->request->is('post'))
      {
        $this->loadModel('Content');
        $data = $this->data;
        //echo "<pre>";print_r($data);die;
        $add['Content']['id'] = 4;
        $add['Content']['content'] = $data['content'];
        $add['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($add);
        $stuff['Content']['id'] = 5;
        $stuff['Content']['content'] = $data['fax'];
        $stuff['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($stuff);
        $stuff['Content']['id'] = 6;
        $stuff['Content']['content'] = $data['email'];
        $stuff['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($stuff);
        $stuff['Content']['id'] = 7;
        $stuff['Content']['content'] = $data['enquiry'];
        $stuff['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($stuff);
        $this->Session->write('success-msg','Content Updated');
        $this->redirect($this->referer());
      }
    }
    public function addFlight($id=null){
      $this->layout="admin";
      $this->loadModel('FlightDetail');
      $this->loadModel('Location');
      $locations = $this->Location->find('all');
      $this->set(compact('locations'));
      if(!empty($id)){
        $id = base64_decode($id);
        $editFlight = $this->FlightDetail->findById($id);
        $this->set(compact('editFlight'));
      }
      if($this->request->is('post'))
      {
        $data = $this->request->data;
        //echo "<pre>";print_r($data);die;
        $data['FlightDetail']['gst_charge'] = (0.18 * $data['FlightDetail']['airfare_charge']);
        $data['FlightDetail']['admin_id'] = $this->Session->read('Admin.id');
        $data['FlightDetail']['created_date'] = date('Y-m-d H:i:s');
        //print_r($data);die;
        $this->FlightDetail->save($data);
        $this->redirect(array('action'=>'flightList'));
      }
    }
    public function flightList(){
      $this->layout="admin";
      $this->loadModel('FlightDetail');
      $this->loadModel('Location');
      $this->FlightDetail->bindModel(
      array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        if(!empty($_GET['from']) || !empty($_GET['to']))
        {
          $from = $_GET['from'];
          $to = $_GET['to'];
          if(!empty($_GET['from']) && !empty($_GET['to']))
          {
            if($this->Session->read('Admin.type') != 0)
              $conditions = array('admin_id'=>$this->Session->read('Admin.id'),'source' => @$from, 'destination' => @$to);
            else
              $conditions = array('source' => @$from, 'destination' => @$to); 
          }
          if(!empty($_GET['from']))
          {
            if($this->Session->read('Admin.type') != 0)
              $conditions = array('admin_id'=>$this->Session->read('Admin.id'),'source' => @$from);
            else
              $conditions = array('source' => @$from);
          }
          if(!empty($_GET['to']))
          {
            if($this->Session->read('Admin.type') != 0)
              $conditions = array('admin_id'=>$this->Session->read('Admin.id'),'destination' => @$to);
            else
              $conditions = array('destination' => @$to);
          }
          $this->Paginator->settings = array('FlightDetail' => array('limit' => 10,'conditions'=> @$conditions));
        }
        else
        {
          if($this->Session->read('Admin.type') != 0)
            $this->Paginator->settings = array('FlightDetail' => array('limit' => 10,'conditions'=> array('admin_id'=>$this->Session->read('Admin.id'))));
          else
            $this->Paginator->settings = array('FlightDetail' => array('limit' => 10));
        }
        $locs = $this->Location->find('all');
        $flight_list=$this->paginate('FlightDetail');
        $this->set(compact('flight_list','locs'));
    }
    public function update_flight_status($id=null){
      $id = base64_decode($id);
        $this->loadModel('FlightDetail');
        $data = $this->FlightDetail->find('first', array('conditions' => array('FlightDetail.id' => $id)));
        if($data['FlightDetail']['status'] == '0'){
            $this->FlightDetail->id = $id;
            $this->FlightDetail->savefield('status',1); 
        }else{
            $this->FlightDetail->id = $id;
            $this->FlightDetail->savefield('status',0); 
        } 
        
        $this->redirect($this->referer());
    }
    public function deleteAll($modal=null,$id=null){
      $id = base64_decode($id);
      $this->loadModel($modal);
      $this->$modal->delete($id);
      $this->redirect($this->referer());
    }
    public function verify($id=null)
    {
      $this->layout = null;
      $id = base64_decode($id);
      $this->loadModel('User');
      $data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
      if($data['User']['varify_status'] == '0')
      {
            $this->User->savefield('varify_status',1); 
        }
      else{
            $this->User->savefield('varify_status',0); 
        } 
    }
    public function addlocation($id=null)
    {
      $this->layout = "admin";
      $this->loadModel('Location');
      if(!empty($id)){
        $location = $this->Location->findById($id);
        $this->set(compact('location'));
      }
      if($this->request->is('post'))
      {
        $data = $this->data;
        //echo "<pre>";print_r($data);die;
        $data['Location']['created_date'] =  date('Y-m-d H:i:s');
        $this->Location->save($data);
        $this->Session->write('success-msg','Location has updated successfully.');
        $this->redirect(array('action'=>'manageLocation'));
        //pr($data);die;
      }
    }

    public function manageLocation(){
      $this->layout = "admin";
      $this->loadModel('Location');
      if(!empty($_GET['keyword']))
      {
        $this->Paginator->settings = array('Location' => array('limit' => 10, 'conditions' => array('location LIKE' => '%'.$_GET['keyword'].'%')));
      }
      else
      $this->Paginator->settings = array('Location' => array('limit' => 10));
      $locations=$this->paginate('Location');
      $this->set(compact('locations'));
    }

    public function contactus()
    {
        $this->layout = "admin";
        $this->loadModel('Contact');
        $this->loadModel('Content');
        $people = $this->Contact->find('all');
        $data['address'] = $this->Content->findById(4);
        $data['fax'] = $this->Content->findById(5);
        $data['email'] = $this->Content->findById(6);
        $data['enquiry'] = $this->Content->findById(7);
        //echo "<pre>";print_r($people);die;
        $this->set(compact('people','data'));
    }

    public function changestatus($id=null)
    {
      $this->layout = null;
       $this->loadModel('Contact');
       $id = base64_decode($id);
       $data = $this->Contact->findById($id);
       $dat['id'] = $id;
      if($data['Contact']['status'] == '0')
      {
            $dat['status'] = '1';
        }
      else{
            $dat['status'] = '0'; 
        } 
        //pr($dat);die;
      $this->Contact->save($dat);
      $this->redirect($this->referer());
    }

     public function aboutus()
    {
        $this->layout = "admin";
        $this->loadModel('Content');
        $content = $this->Content->findByType('1');
        $this->set(compact('content'));
        if($this->request->is('post'))
        {
            $data = $this->data;
            $dat['id'] = 1;
            $dat['content'] = $data['content'];
            $this->Content->save($dat);
            $this->redirect($this->referer());
        }
        
    }

    public function tc()
    {
        $this->layout = "admin";
        $this->loadModel('Content');
        $content = $this->Content->findByType('2');
        $this->set(compact('content'));
        if($this->request->is('post'))
        {
            $data = $this->data;
            $dat['id'] = 2;
            $dat['content'] = $data['content'];
            $this->Content->save($dat);
            $this->redirect($this->referer());
        }
        
    }

    public function privacy()
    {
        $this->layout = "admin";
        $this->loadModel('Content');
        $content = $this->Content->findByType('3');
        $this->set(compact('content'));
        if($this->request->is('post'))
        {
            $data = $this->data;
            $dat['id'] = 3;
            $dat['content'] = $data['content'];
            $this->Content->save($dat);
            $this->redirect($this->referer());
        }
        
    }    
    public function content()
    {
      $this->layout = "admin";
      $this->loadModel('Content');
      $content = $this->Content->findByType('8');
      $content1 = $this->Content->findByType('9');
      $content2 = $this->Content->findByType('10');
      //echo "<pre>";print_r($content);print_r($content1);print_r($content2);die;      
      $this->set(compact('content','content1','content2'));
      if($this->request->is('post'))
      {
        $data = $this->data;
        //echo "<pre>";print_r($data);die;
        $dat['Content']['id'] = 8;
        $dat['Content']['content'] = $data['desc'];
        $dat['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($dat);
        $dat['Content']['id'] = 9;
        $dat['Content']['content'] = $data['cont1'];
        $dat['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($dat);
        $dat['Content']['id'] = 10;
        $dat['Content']['content'] = $data['cont2'];
        $dat['Content']['updated date'] = date("Y-m-d");
        $this->Content->save($dat);
        $this->Session->write('success-msg','Customer Support Content Updated!');
        $this->redirect($this->referer());
      }
    }
    public function packages()
    {
      $this->layout = "admin";
      $this->loadModel('Package');
      $specials = $this->Package->findAllByType(1);
      $holidays = $this->Package->findAllByType(2);
      //echo "<pre>";print_r($specials);die;
      $this->set(compact('specials','holidays'));
    }
    public function adverts()
    {
      $this->layout = "admin";
      $this->loadModel('Advertisement');
      $mainimage = $this->Advertisement->findByType(1);
      $discounts = $this->Advertisement->findAllByType(2);
      $end = $this->Advertisement->findByType(3);
      $this->set(compact('mainimage','discounts','end'));
      if($this->request->is('post'))
      {
        $data = $this->data;
        //echo "<pre>";print_r($data);print_r($_FILES);die;
        if (!empty($_FILES['image']['name'])) 
        {
          $file = realpath('../webroot/img/Ads/').'/';
          if(move_uploaded_file($_FILES['image']['tmp_name'], $file.$_FILES['image']['name']))
            {
              $file1['Advertisement']['id'] = $data['id'];
              $file1['Advertisement']['image'] = $_FILES['image']['name'];
              //$this->Package->create();
              $this->Advertisement->save($file1);
            }
         }     
        $this->Session->write('success-msg','Advertisement Updated!');
        $this->redirect(array('action' => 'adverts'));   
      }
    }
    public function setdefault($id=null)
    {
      $this->loadModel('Advertisement');
      $def['Advertisement']['id'] = $id;
      $def['Advertisement']['image'] = "coming.jpg";
      $this->Advertisement->save($def);
      $this->redirect($this->referer());
    }
    public function uploadspecials($id=null)
    {
      $this->layout = "admin"; 
      $id = base64_decode($id);
      $this->loadModel('Package');
      $one = $this->Package->findById($id);
      $this->set(compact('one'));
      //echo "<pre>";print_r($one);die;
      if($this->request->is('post'))
      {
        $data = $this->data;
        $data['Package']['id'] = $id;
        //echo "<pre>";print_r($_FILES);die;
        $this->Package->save($data);
        if (!empty($_FILES['image']['name'])) 
        {
          $file = realpath('../webroot/img/Ads/').'/';
          if(move_uploaded_file($_FILES['image']['tmp_name'], $file.$_FILES['image']['name']))
            {
              $file1['Package']['id'] = $id;
              $file1['Package']['image'] = $_FILES['image']['name'];
              //$this->Package->create();
              $this->Package->save($file1);
            }
         }     
        $this->Session->write('success-msg','Package Updated!');
        $this->redirect(array('action' => 'packages'));
      }
    }
    public function bookings()
    {
      $this->layout = "admin";
      $this->loadModel('Order');
      $this->loadModel('User');
      $this->loadModel('Location');
      $this->loadModel('FlightDetail');

      $this->Order->bindModel(array('belongsTo' => array('User' => array('className' => 'User','foreignKey' => 'user_id'))));
      $this->Order->bindModel(array('belongsTo' => array('Flight' => array('className' => 'FlightDetail','foreignKey' => 'flight_id'))));
      $this->Order->bindModel(array('belongsTo' => array('ReturnFlight' => array('className' => 'FlightDetail','foreignKey' => 'Flight_id_return'))));
      $this->Order->bindModel(array('belongsTo' => array('From' => array('className' => 'Location','foreignKey' => 'source'))));
      $this->Order->bindModel(array('belongsTo' => array('To' => array('className' => 'Location','foreignKey' => 'destination'))));
      if(!empty($_GET['from']) || !empty($_GET['to']))
      {
        $from = $_GET['from'];
        $to = $_GET['to'];
        if(!empty($_GET['from']) && !empty($_GET['to']))
        {
          if($this->Session->read('Admin.type') != 0)
            $conditions = array('Order.admin_id'=>$this->Session->read('Admin.id'),'Order.source' => @$from, 'Order.destination' => @$to);
          else
            $conditions = array('Order.source' => @$from, 'Order.destination' => @$to); 
        }
        if(!empty($_GET['from']))
        {
          if($this->Session->read('Admin.type') != 0)
            $conditions = array('Order.admin_id'=>$this->Session->read('Admin.id'),'Order.source' => @$from);
          else
            $conditions = array('Order.source' => @$from);
        }
        if(!empty($_GET['to']))
        {
          if($this->Session->read('Admin.type') != 0)
            $conditions = array('Order.admin_id'=>$this->Session->read('Order.Admin.id'),'Order.destination' => @$to);
          else
            $conditions = array('Order.destination' => @$to);
        }
        $this->Paginator->settings = array('Order' => array('limit' => 10,'conditions'=> @$conditions,'recursive' => 2,'order' => array('Order.id' => 'DESC')));
      }
      else
      {
        if($this->Session->read('Admin.type') != 0)
          $this->Paginator->settings = array('Order' => array('limit' => 10,'recursive' => 2,'order' => array('Order.id' => 'DESC'),'conditions'=> array('Order.admin_id'=>$this->Session->read('Admin.id'))));
        else
          $this->Paginator->settings = array('Order' => array('limit' => 10,'recursive' => 2,'order' => array('Order.id' => 'DESC')));
      }
      $orders=$this->paginate('Order');
      $locs = $this->Location->find('all');
      $this->set(compact('orders','locs'));
      //echo "<pre>";print_r($orders);die;
    }
    public function userorders($id=null)
    {
      $this->layout = "admin";
      $this->loadModel('Order');
      $this->loadModel('User');
      $this->loadModel('Location');
      $this->loadModel('FlightDetail');
      $this->loadModel('Passenger');
      $id = base64_decode($id);
      $this->Order->bindModel(array('belongsTo' => array('User' => array('className' => 'User','foreignKey' => 'user_id'))));
      $this->Order->bindModel(array('belongsTo' => array('Flight' => array('className' => 'FlightDetail','foreignKey' => 'flight_id'))));
      $this->Order->bindModel(array('belongsTo' => array('ReturnFlight' => array('className' => 'FlightDetail','foreignKey' => 'Flight_id_return'))));
      $this->Order->bindModel(array('belongsTo' => array('From' => array('className' => 'Location','foreignKey' => 'source'))));
      $this->Order->bindModel(array('belongsTo' => array('To' => array('className' => 'Location','foreignKey' => 'destination'))));
      $this->Order->bindModel(array('hasMany' => array('Passengers' => array('className' => 'Passengers','foreignKey' => 'order_id'))));
      $this->Paginator->settings = array('Order' => array('limit' => 10,'recursive' => 2,'conditions' => array('Order.user_id' => $id),'order' => array('Order.id' => 'DESC')));
      $orders = $this->paginate('Order');
      $this->set(compact('orders'));
      //echo "<pre>";print_r($orders);die;
    }
  }
