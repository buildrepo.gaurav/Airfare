<?php 

//App::import('Vendor', 'payu');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');
ob_start();
class HomeController extends AppController
{
     public $helpers = array('Session','Html','Js','Form','Paginator');
	   	public $components = array(
						'Session','Email','RequestHandler','Cookie','Paginator', 
						'Auth' => array(
							'loginAction' => array('controller'=>'home', 'action'=>'index'),
							'loginRedirect' => array('controller' => 'home', 'action' => 'index'),
								'logoutRedirect' => array('controller' => 'home', 'action' => 'index'),
								'authenticate' => array('all' => array('scope' => array('User.status' => 1)),'Form' => array('fields' => array('username' => 'email'))),
								'authError'=>"You can't access that page",
								'authorize'=>array('Controller')
							),
						);

	 public function isAuthorized($user)
   {
   	return true;
   }
		public function beforeFilter()    	{	
    	
      parent::beforeFilter();	 

        //  if($this->params['action'] == 'bookFlight'){
        //    if(!$this->Session->check('Auth.User') ){

        //         $this->redirect(array('action' => 'index'));
        //     }
        // }
      	$this->Auth->allow('flight_list','ajaxrequest','login','index','signup','activation_link','logout','successful','forgotpassword','my_search','product_details','contacts','feedback','faq','privacy_policy','about_us','term','profile_details','verify','verify_link','businesses','header_search','product_count','business_offers','fav_business','sitemap','shipadd', 'register','varify', 'contactus','aboutus','termsconditions','resetpassword','privacy','search_box','search_box2'); 
        $allow = array('flight_list','ajaxrequest','login','index','signup','activation_link','logout','successful','forgotpassword','my_search','product_details','contacts','feedback','faq','privacy_policy','about_us','term','profile_details','verify','verify_link','businesses','header_search','product_count','business_offers','fav_business','sitemap','shipadd', 'register','varify', 'contactus','aboutus','termsconditions','resetpassword','privacy','search_box','search_box2');
        if(!in_array($this->params['action'], $allow)){
      
           if(!$this->Session->check('Auth.User')){
            
                $this->redirect(array('action'=>'index'));
            }
        }   
        $this->loadModel('Location');
        $this->loadModel('Package');
        $this->loadModel('Advertisement');
        $countcity = $this->Location->find('all');
        $this->loadModel('Content');
        $user = $this->Session->read('Auth');
        //pr($user);die;
        $content = $this->Content->findByType('8');
        $content1 = $this->Content->findByType('9');
        $content2 = $this->Content->findByType('10');
        $specials = $this->Package->findAllByType('1');
        $holidays = $this->Package->findAllByType('2');
        $ads = $this->Advertisement->find('all');
        //echo "<pre>";print_r($ads);die;
        $this->set(compact('content','content1','content2','specials','holidays','ads','user'));
        //pr($countcity);die;
      }	

 function index()
 {
   $this->layout = 'frontend';
   $this->loadModel('Location');
   $locations = $this->Location->find('all');
   $this->Session->write('Testing', 'Successfully Verified!!');
   $this->set(compact('locations'));
 }
 function flight_list()
 {
  $this->layout = 'frontend';
  $this->loadModel('FlightDetail');
  $this->loadModel('Location');
      if ($this->request->is('post')) {
        $search = $this->request->data;
        //pr($search);die;
        $locations = $this->Location->find('all');
        $depart = date("Y-m-d H:i:s", strtotime($search['depart_date']." 00:00:00"));
        $departEnd = date("Y-m-d H:i:s", strtotime($search['depart_date']." 23:59:59"));
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        
        
          $count = $this->FlightDetail->find('all', array('conditions' => array('FlightDetail.source' => $search['place_from'], 'FlightDetail.destination' => $search['place_to'], 'datetime_departure >=' => $depart,'datetime_departure <=' => $departEnd)));
          //pr($count);die;
        $flag = 0;
       if($search['type_journey'] == 'RoundTrip')
       {  
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        $return = date("Y-m-d H:i:s", strtotime($search['return_date']." 00:00:00"));
        $returnEnd = date("Y-m-d H:i:s", strtotime($search['return_date']." 23:59:59"));
            $countreturn = $this->FlightDetail->find('all', array('conditions' => array('FlightDetail.destination' => $search['place_from'], 'FlightDetail.source' => $search['place_to'], 'datetime_departure >=' => $return,'datetime_departure <=' => $returnEnd)));
            $flag = 1;
        }
      //print_r($search);
     //pr($count);di
      if(empty($count))
        $flag = 2;
      $this->set(compact('locations','countreturn', 'count', 'search', 'flag'));
       //pr($flag);die;
    }
 }

 function login()
 {
   
      if($this->request->is('post'))
      {
        $this->loadModel('User');
        $data = $this->request->data;
        
        $data['password'] = md5($data['password']);
        $result = $this->User->find('first', array('conditions' => array('User.email' => $data['email'], 'User.password' => $data['password'], 'User.varify_status' => 1)));
        //pr($result); die;
        if(!empty($result)){
          $this->Session->write('Auth',$result);
          $this->redirect(array('action'=>'dashboard'));
        }else{
          $this->Session->write('verified', 'Invalid credentials or unregistered user.');
          $this->redirect(array('action'=>'index'));
        }
        
      }

 }
public function logout(){
      $this->Session->destroy('Auth');
      $this->redirect(array('action'=>'index'));
   }

   public function resetpassword($id=null)
   {
      $this->layout = 'frontend';
      $this->loadModel('User');
      $id = base64_decode($id);
      
      if($this->request->is('post'))
      {
        
        $dat = $this->data;
        $data['id'] = $id;
        $data['password'] = md5($dat['newpassword']);
        $this->User->save($data); 
        $this->Session->write('verified', 'Password Successfully Reset.');
        $this->redirect(array('action' => 'index'));
    }
   }
   public function forgotpassword()
   {
        $this->layout = 'frontend';
        $this->loadModel('User');
        if($this->request->is('post'))
        {
        $data = $this->data;
        //pr($data);die;
        $user = $this->User->findByEmail($data['email']);
        //pr($user);die;
        $link = HTTP_ROOT.'Home/resetpassword/'.base64_encode($user['User']['id']);
                $mail = 'Greetings!<br>Please use the link to reset your password<br><a href="'.$link.'">Click Here</a>';
                $Email = new CakeEmail();
                $Email->emailFormat('html');
                $Email->from(array('yogesh.k@globiztechnology.com'));
                $Email->to($user['User']['email']);
                $Email->subject('Password Reset');
                $Email->send($mail);
            }
   }
 function dashboard()
 {
    $this->layout = 'frontend';
    $this->loadModel('Passenger');
    $this->loadModel('Order');
    $this->loadModel('FlightDetail');
    $this->loadModel('Location');
    $this->loadModel('User');
    $this->Order->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );
    if(!empty($this->Session->read('repay.Balance')))
      $this->Session->delete('repay');
    $user = $this->User->findById($this->Session->read('Auth.User.id'));
    $find = $this->Order->find('all', array('limit' => '5', 'order' => array(
    'Order.id DESC') ,'conditions' => array('Order.user_id' => $this->Session->read('Auth.User.id'))));

    // $flight = $this->FlightDetail->findById($find['Order']['flight_id']);
    // $source = $this->Location->findById($find['Order']['source']);
    // $destin = $this->Location->findById($find['Order']['destination']);
    //pr($find);die;
    // pr($flight);
    // pr($source);
    // pr($destin);
    //die;
    $this->set(compact('find', 'user'));
 }
  function search_box($letter=null)
  {
    $this->loadModel('Location');
    $conditions = array('OR' => array(array('Location.location LIKE' => '%'.$letter.'%'),array('Location.code LIKE' => '%'.$letter.'%')));
    $places = $this->Location->find('all', array('conditions' => @$conditions));
    $resp = '';
    //pr($people);die;
    foreach($places as $pla)
    {
        $resp = $resp . '<li onclick="fn('."'".$pla['Location']['location']."'".','.$pla['Location']['id'].')" style="cursor:pointer">'.$pla['Location']['location'].' ('.$pla['Location']['code'].')'.'</li>';
    } 
    if(!empty($resp))
    {
      echo $resp;
      die;
    }
    else
    {
      echo "<li>No Results Found</li>";
      die;
    }
  }
  function search_box2($letter=null)
  {
    $this->loadModel('Location');
    $conditions = array('OR' => array(array('Location.location LIKE' => '%'.$letter.'%'),array('Location.code LIKE' => '%'.$letter.'%')));
    $places = $this->Location->find('all', array('conditions' => @$conditions));
    $resp = '';
    //pr($people);die;
    foreach($places as $pla)
    {
        $resp = $resp . '<li onclick="fs('."'".$pla['Location']['location']."'".','.$pla['Location']['id'].')" style="cursor:pointer">'.$pla['Location']['location'].' ('.$pla['Location']['code'].')'.'</li>';
    } 
    if(!empty($resp))
    {
      echo $resp;
      die;
    }
    else
    {
      echo "<li>No Results Found</li>";
      die;
    }
  }
 function register()
 {
    $this->loadModel('User');
    $data = $this->request->data;
    //pr($data); die;
    $data['created_date'] = date('Y-m-d H:m:s');
    $data['varify_status'] = 0;
    $pass= $data['password'];
    $data['password'] = md5($pass);
    $match = $this->User->find('first', array('conditions' => array('User.email' => $data['email'])));
    if($match)
    {
        $this->Session->write('verified', 'Email Already Registered');
    }
    else
    {
      if($this->User->save($data))
          { 
                //pr($data);die;
                $id = $this->User->getLastInsertId();
                $link = HTTP_ROOT.'Home/varify/'.base64_encode($id);
                $mail = 'Greetings!<br>Your Account has been successfully created on Airfare.<br>Please click on the link below to verify your account<br><a href="'.$link.'">Click Here</a><br>Please use you email to login after verification<br><b>Password: </b>'. $pass;
                $Email = new CakeEmail();
                $Email->emailFormat('html');
                $Email->from(array('yogesh.k@globiztechnology.com'));
                $Email->to($data['email']);
                $Email->subject('Account Created');
                $Email->send($mail);
                $this->Session->write('verified', 'Registered Sucessfully! Verify your account to login.');
          }
          else
          {
            $this->Session->write('verified', 'Oops! Something Went Wrong, Try again.');
          }
    }
    $this->redirect(array('action' => 'index'));
 }
 public function verify($id=null)
    {
      $this->layout = null;
      $id = base64_decode($id);
      //pr($id);die;
      $this->loadModel('User');
      $data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
      if($data['User']['varify_status'] == '0')
      { 
            $this->User->id = $id;
            $this->User->savefield('varify_status',1); 
        }
      else{
            $this->User->id = $id;
            $this->User->savefield('varify_status',0); 
        } 
    }

  public function bookFlight(){
    $this->layout = 'frontend';
    $this->loadModel('FlightDetail');
    $this->loadModel('User');
    $this->loadModel('Location');
    $this->loadModel('Order');
    $this->loadModel('Passenger');
    $data = $this->data;
    //pr($data);die;
    $cost = $this->FlightDetail->findById($data['onwardFlight']);
    $order['journey_type'] = $data['type_journey'];
    $order['flight_id'] = $data['onwardFlight'];
    $order['passengers'] = $data['adult'] + $data['child'] + $data['infant'];
    $flight = $this->FlightDetail->findById($order['flight_id']);
    $order['amount'] = ($order['passengers'] * ($cost['FlightDetail']['cost'] + $cost['FlightDetail']['airfare_charge'] + $cost['FlightDetail']['gst_charge']));
    //***************************************************************************************
    if($order['journey_type'] == 'RoundTrip')
    {
        $order['Flight_id_return'] = $data['returnFlight'];
        $flight_return = $this->FlightDetail->findById($order['Flight_id_return']);
        if(!($flight_return['FlightDetail']['seat_availability'] >= $order['passengers']))
        {
            $this->Session->write('verified', 'Not enough seats available in return trip.');
            $this->redirect(array('action' => 'index'));
        }
        $cost_return = ($order['passengers'] * ($flight_return['FlightDetail']['cost'] + $flight_return['FlightDetail']['airfare_charge'] + $flight_return['FlightDetail']['gst_charge']));
        $order['amount'] =  $order['amount'] + $cost_return;
        //update seats
        $seats_return['id'] = $order['Flight_id_return'];
        $seats_return['seat_availability'] = $flight_return['FlightDetail']['seat_availability'] - $order['passengers'];
        $this->FlightDetail->save($seats_return);
    }
    else
    {
        $flight_return = 0;
    }
    //***************************************************************************************

    
    //*********************************************
    if(!($flight['FlightDetail']['seat_availability'] >= $order['passengers']))
    {
        $this->Session->write('verified', 'Not enough seats available. Try a different flight!');
        $this->redirect(array('action' => 'index'));
    }
    //**********************************************
    
    $order['source'] = $data['place_from'];
    $order['destination'] = $data['place_to'];
    
    $order['date'] = $data['depart_date'];
    $order['return_date'] = $data['return_date'];
    
    //$order['order_number'] = 'OR00';
    $order['created_datetime'] = date('Y-m-d H:i:s');
    
    $order['user_id'] =  $this->Session->read('Auth.User.id');
    $order['status'] = 0;
    $order['admin_id'] = $flight['FlightDetail']['admin_id'];
    //**************************************************
    //Seats Updation
    $seats['id'] = $order['flight_id'];
    $seats['seat_availability'] = $flight['FlightDetail']['seat_availability'] - $order['passengers'];
    $this->FlightDetail->save($seats);
    //**************************************************
    //pr($order);die;
    $this->Order->save($order);
    $order_id = $this->Order->getLastInsertID();
    $order1['id'] = $order_id;
    $order1['order_number'] = 'OR00'.$order_id;
    $this->Order->save($order1);
    $source = $this->Location->findById($order['source']);
    $destination = $this->Location->findById($order['destination']);
    $u = $this->Session->read('Auth.User.id');
    $wallet = $this->User->findById($u);
    $wallet = $wallet['User']['wallet'];
    //pr($wallet);die;
    //pr($flight_return);die;
    $this->set(compact('order', 'source', 'destination', 'flight', 'order_id', 'flight_return','wallet'));
    // pr($order);
    // pr($source);
    // pr($destination);
    // pr($flight);
    // die;  
  }  
  public function passengers($id=null)
  {
    $this->layout = 'frontend';
    $this->loadModel('Passenger');
    $this->loadModel('Order');
    $this->loadModel('User');
    $this->loadModel('FlightDetail');
    $this->loadModel('Location');
    $id = base64_decode($id);
    if($this->request->is('post'))
    {
      //pr($this->data);die;
      $totalamount = 0;
      $pass = $this->data['Pass'];
      $user = $this->Session->read('Auth');
      if($user['User']['type'] == 2)
      {
        $ord = $this->Order->findById($id);
        $new['Order']['id'] = $id;
        $new['Order']['amount'] = $ord['Order']['amount'] + $this->data['charges'];
        $totalamount = $new['Order']['amount'];
        //pr($new);die;
        $this->Order->save($new);
      }
      if($user['User']['type'] == 1)
      {
        $ord = $this->Order->findById($id);
        $totalamount = $ord['Order']['amount'];
      }
      foreach($pass as $passenger)
      {
        $passenger['Passenger']['order_id'] = $id;
        $passenger['Passenger']['created_date'] = date("Y-m-d H-i-s");
        //pr($passenger);die;
        $this->Passenger->create();
        $this->Passenger->save($passenger);
      }
      //pr($pass);die;
    }
    
    $this->Order->bindModel(
        array('hasMany' => array(
                'Passengers' => array(
                    'className' => 'Passenger',
                    'foreignKey' => 'order_id'
                    )
                )
            )
        );
    
    if(!empty($this->data['wallet']))
      {
        if($this->data['wallet'] == 1)
        {
          //pr($this->data['wallet']);die;
          $u = $this->Session->read('Auth.User.id');
          $user = $this->User->findById($u);
          if($user['User']['wallet'] >= $totalamount)
          {
            $wall['User']['wallet'] = $user['User']['wallet'] - $totalamount;
            $wall['User']['id'] = $u;
            $this->User->save($wall);
          }
          else
          {
            $bal = $totalamount - $user['User']['wallet'];
            $wall['User']['wallet'] = 0;
            $wall['User']['id'] = $u;
            $this->User->save($wall);
            $save['OrderID'] = $id;
            $save['Balance'] = $bal;
            $this->Session->write('repay', $save);
            $this->redirect(array('action' => 'addwallet'));
          }
        }
      }
    $info = $this->Order->findById($id);
    $source = $this->Location->findById($info['Order']['source']);
    $destination = $this->Location->findById($info['Order']['destination']);
    $flight = $this->FlightDetail->findById($info['Order']['flight_id']);
    $this->set(compact('info', 'source', 'destination', 'flight'));
    $status['id'] = $id;
    $status['status'] = 1;
    $this->Order->save($status);
    
    //pr($info);die;
    // pr($source);
    // pr($destination);
    // pr($flight);
    // die;
  }
  public function addwallet($id=null)
  {
    $this->layout = 'frontend';
    $this->loadModel('User');
    $this->loadModel('Order');
    if(!empty($this->Session->read('repay.Balance')))
    {
      $data = $this->Session->read('repay');
      //pr($data);die;
      $bal = $data['Balance'];
    }
    $u = $this->Session->read('Auth.User.id');
    $wallet = $this->User->findById($u);
    $wallet = $wallet['User']['wallet'];
    $this->set(compact('wallet','bal'));

    if($this->request->is('post'))
    {
      $data = $this->data;
      if(!empty($this->Session->read('repay.Balance')))
      {
        $status['id'] = $this->Session->read('repay.OrderID');
        $status['status'] = 1;
        $this->Order->save($status);
        $OrderID = $this->Session->read('repay.OrderID');
        $this->redirect(array('action' => 'passengers/'.base64_encode($OrderID)));
      }      
      //pr($data);die;
      $add['User']['id'] = $u;
      $add['User']['wallet'] = $wallet + $data['amount'];
      $this->User->save($add);
      $this->redirect($this->referer());
    }
   }
  public function varify($id=null){
      $this->layout=null;
      $id = base64_decode($id);
     // echo $id;die;
      $this->loadModel('User');
      $res = $this->User->findById($id);
      if($res['User']['varify_status'] == 0)
      {
        $this->User->id = $id;
        $this->User->savefield('varify_status', 1);
        $this->Session->write('verified', 'Successfully Verified!!');
    }
    else
    {
      $this->Session->write('verified', 'Already Verified!! Proceed to Login');
    }
    $this->redirect(array('action' => 'index'));
  }


  public function editprofile($id=null)
  {
    $id = base64_decode($id);
    $this->layout = 'frontend';
    $this->loadModel('User');
    $data = $this->User->findById($id);
    if($this->request->is('post'))
    {
      //pr($_FILES);die;
      $edit = $this->request->data;
      $edit['id'] = $id;
      App::Import('Component','UploadComponent');
          $imgName=pathinfo($_FILES['image']['name']);
          $ext=$imgName['extension'];
          $newImgName = rand(10,100000);
          $tempFile = $_FILES['image']['tmp_name'];
          $destination = realpath('../webroot/img/profile/'). '/';
                   //pr($_FILES);die;
          if(is_uploaded_file($_FILES['image']['tmp_name']))
            {

             $src = $_FILES['image']['tmp_name'];
              list( $width, $height, $source_type ) = getimagesize($src);
                        
              move_uploaded_file($_FILES['image']['tmp_name'],$destination.$newImgName.".".$ext);
              }
              $Image['User']['image']=$newImgName.".".$ext;
              $Image['User']['id'] = $id;
              $this->User->save($Image);
      //pr($edit);die;
      $this->User->save($edit);
      $this->redirect($this->referer());
    }

    //pr($data);die;
    $this->set(compact('data'));
  }
  public function changepassword($id=null)
  {
    $this->layout = 'frontend';
    $this->loadModel('User');
    $id = base64_decode($id);
    $data = $this->User->findById($id);
    //pr($data);die;
    if($this->request->is('post'))
    {

      $change = $this->request->data;
      $change['oldpassword'] = md5($change['oldpassword']);
      $change['newpassword'] = md5($change['newpassword']);
      $change['confirmnewpassword'] = md5($change['confirmnewpassword']);
      if($data['User']['password'] == $change['oldpassword'])
      {
        if($change['newpassword'] == $change['confirmnewpassword'])
        {
          $save['password'] = $change['newpassword'];
          $save['id'] = $id;
          //pr($save);die;
          $this->User->save($save);
          $this->redirect(array('action' => 'dashboard'));
        }
      }
      //pr($change);die;
    }
  }
  public function check_password(){
      $this->loadModel('User');
      //pr($_POST);die;
      if($this->Session->read('Auth.User.password') == md5($_POST['oldpassword'])){
        echo 'true';
      }
      else{
        echo 'false';
      }
      die;
    }

    public function aboutus()
    {
      $this->layout = 'frontend';
      $this->loadModel('Content');
      $content = $this->Content->findByType('1');
      $this->set(compact('content'));
    }
    public function contactus()
    {
      $this->layout = 'frontend';
      $this->loadModel('Contact');
      $this->loadModel('Content');
      $data['address'] = $this->Content->findById(4);
      $data['fax'] = $this->Content->findById(5);
      $data['email'] = $this->Content->findById(6);
      $data['enquiry'] = $this->Content->findById(7);
      $this->set(compact('data'));
      if($this->request->is('post'))
      {
          $data = $this->data;
          $this->Contact->save($data);
          $this->Session->write('verified', 'Thanks! We will contact you soon!');
      }
    }
    public function termsconditions()
    {
      $this->layout = 'frontend';
      $this->loadModel('Content');
      $content = $this->Content->findByType('2');
      $this->set(compact('content'));

    }

    public function privacy()
    {
      $this->layout = 'frontend';
      $this->loadModel('Content');
      $content = $this->Content->findByType('3');
      $this->set(compact('content'));

    }
    public function bookinghistory()
    {
      $this->layout = 'frontend';
      $this->loadModel('Order');
      $this->loadModel('Passenger');
      $id = $this->Session->read('Auth.User.id');
      $this->Order->bindModel(
        array('hasMany' => array(
                'Passengers' => array(
                    'className' => 'Passenger',
                    'foreignKey' => 'order_id'
                    )
                )
            )
        );
      $this->Order->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight_onward' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );$this->Order->bindModel(
        array('belongsTo' => array(
                'Flight_return' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'Flight_id_return'
                    )
                )
            )
        );
      $find = $this->Order->find('all', array('limit' => '50', 'order' => array(
    'Order.id DESC') ,'conditions' => array('Order.user_id' => $id)));
      //pr($find);die;
    $this->set(compact('find'));
    }

    public function invoice($id=null)
    {
      $this->layout = 'frontend';
      $this->loadModel('Order');
      $this->loadModel('Passenger');
      $id = base64_decode($id);
      $this->Order->bindModel(
        array('hasMany' => array(
                'Passengers' => array(
                    'className' => 'Passenger',
                    'foreignKey' => 'order_id'
                    )
                )
            )
        );
      $this->Order->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight_onward' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );
    $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight_return' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'Flight_id_return'
                    )
                )
            )
        );
        $find = $this->Order->findById($id);
        //pr($find);die;
        $this->set(compact('find'));
    }
} 
?>

