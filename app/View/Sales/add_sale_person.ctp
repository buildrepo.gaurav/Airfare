
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
          <?php if(!empty($id)){?>
            <div class="h2 page-heading">Edit Sales Executive</div>
          <?php }else{?>
            <div class="h2 page-heading">Add Sales Executive</div>
          <?php }?>  
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <form method="post" enctype="multipart/form-data" id="sales-form">
        <div class="block block-bordered">
            <div class="form-horizontal block-content">
                
                

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">UserName<span class="text-danger">*</span></label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[Admin][username]" id="name" value="<?php echo @$data['Admin']['username'];?>" maxlength="50" required>
                <input type="hidden" name="data[Admin][id]" id="id" value="<?php echo @$data['Admin']['id'];?>">
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Email<span class="text-danger">*</span></label>
                </div>
                <div class="col-md-8">
                <input class="form-control required email" type="email" name="data[Admin][email]" id="email" value="<?php echo @$data['Admin']['email'];?>" maxlength="50" required>
                </div></div>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Phone<span class="text-danger">*</span></label>
                </div>
                <div class="col-md-8">
                <input class="form-control required number" type="text" name="data[Admin][phone]" id="phone" value="<?php echo @$data['Admin']['phone'];?>" maxlength="50" required>
                </div></div>

                <?php if(empty($id)){?>
                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Password<span class="text-danger">*</span></label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="password" name="data[Admin][password]" id="password" value="" maxlength="50" required>
                </div></div>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Confirm Password<span class="text-danger">*</span></label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="password" id="confirm-password" maxlength="50" value="" required>
                </div></div>

                <?php }?>


                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 
  <script>

$(document).ready(function () {
    $('#sales-form').validate();
    // $('.dataTables_filter').find('input').addClass('.form-control');

/*    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#role').on('change', function() {
        var role = $('#role').val();
        if(role == 1){
            $("#ware").css("display", "block");
        }
        else if(role == 0){
            $("#ware").css("display", "block");
        }
        else{
            $("#ware").css("display", "none");    
        }
        
    });*/
});
</script>