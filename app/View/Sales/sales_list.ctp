  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  .search_for
  {
    margin-top: 4%;
    margin-bottom: 2%;
  }
  .proper
  {
    text-align: right;
    padding-top: 1%;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Sales Executives List</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Sales Executives</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Sales Executives</div>
              </div>
                    <div class="pull-right">
                    <a href="<?php echo HTTP_ROOT?>Sales/addSalePerson" class="btn btn-default"><i class="fa fa-plus"></i> Add Sales Executives</a>
                    </div>
              <br>
              <form action="" method="get">
                <div class="row search_for">
                  <div class="col-md-2 proper"><b>Search:</b></div>
                  <div class="col-md-3">
                    <input type="text" name="keyword" class="form-control" placeholder="Search Executive">
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn btn-danger">Search</button>
                  </div>
                </div>
              </form> 
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                        <tr role="row">
                            <th>SNo<i class="sorting_icon"></i></th>
                            <th>Email<i class="sorting_icon"></i></th>
                            <th>Username<i class="sorting_icon"></i></th>
                            <th>Phone<i class="sorting_icon"></i></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php if(!empty($user_list)){ $i=1; foreach($user_list as $user){?>
                        <tr class="odd">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $user['Admin']['email']?></td>

                            <td><?php echo $user['Admin']['username']?></td>

                            <td><?php echo $user['Admin']['phone']?></td>
                            
                            <td>
                              <a title="Update Status" href="<?php echo HTTP_ROOT.'Sales/addSalePerson/'.$user['Admin']['id']?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                              <a title="Delete" onclick="if(!confirm('Are you sure, you want to delete this Executive?')){return false;}" style="color: red;" href="<?php echo HTTP_ROOT.'Sales/delete/'.$user['Admin']['id']?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++;}} else{?>
                      <tr><td colspan="9"><b>No Records</b></td></tr>
                      <?php }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
