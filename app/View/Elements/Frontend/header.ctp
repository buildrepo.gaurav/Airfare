
<style type="text/css">
      .navbar_main
      {
        margin-right: 0%!important;
      }
    </style>
<section class="form-section parallax">
<section>
 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header" style="margin-left: 4%;">
      <a class="navbar-brand " style="font-size: 30px;" href="<?php echo HTTP_ROOT?>"><img src="<?php echo HTTP_ROOT?>img/various/logo.png" style="width: 26%;"></a>
    </div>
    <ul class="nav navbar-nav navbar-right navbar_main">
    <?php if(!($this->Session->check('Auth'))) { ?>
    
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><button type="button" class="btn btn-warning Login-button" data-backdrop="false" data-toggle="modal" data-target="#id01">Login</button>
      <!-- <div id="signup">New Customer <a id="signup_link" data-backdrop="false" data-toggle="modal" data-target="#id02">SIGNUP</a> here<div> -->
      </li>
       <li class="divider"></li> 
          <li><button type="button" class="btn btn-warning Login-button" data-backdrop="false" data-toggle="modal" data-target="#id02">Register</button></li>
      <!-- <li class="divider"></li> 
          <li><a href="#">Agent Login</a></li>
      <li class="divider"></li> 
      <li><a href="#">My Bookings</a></li> -->
        </ul>
      </li>
      <?php } ?>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>">Home</a></li>
      <?php if(($this->Session->check('Auth'))) { ?>
        <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/dashboard">Dashboard</a></li>
      <?php } ?>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/aboutus">About Us</a></li>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/termsconditions">T&C</a></li>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/contactus">Contact Us</a></li>
      
       <?php if(($this->Session->check('Auth'))) { ?>
      <li>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><?php 
            $str = $this->Session->read('Auth.User.name');
            $name = explode(" ",$str);
            echo $name['0']; 
          ?>
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li class="dropdown-header">Profile</li>
            <li><a href="<?php echo HTTP_ROOT?>Home/editprofile/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Edit Profile</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/changepassword/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Change Password</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/bookinghistory/">Booking History</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/logout">Logout</a></li>
          </ul>
        </div>
      </li>
      <?php } ?>
    </ul>
  </div>
</nav> 
</section>

<?php 

$title=$this->params['action'];
if($title != 'index'){ ?>
<style type="text/css">
  .parallax
  {
    background-image: none;
    min-height: 0px; 
  }
  .navbar-inverse
{
  background-color: rgba(0, 0, 0, 1) !important;
  border: none;
  
}
</style>
<?php } ?>

<?php 
if($title == 'index'){ ?>

    <section class="form_main">
    <div class="row">
     <form id="search-form" action="<?php echo HTTP_ROOT?>Home/flight_list" method="post">
      <div class="col-sm-10 formforsearch">
        <br>
        <div class="row">
          <div class="col-sm-12 form_element">
          <label class="radio-inline"><input type="radio" value="OneWay" id="OneWay"  name="type_journey" checked>One Way</label>
          <label class="radio-inline"><input type="radio" value="RoundTrip" id="RoundTrip"  name="type_journey">Round Trip</label>
          </div>
          
        </div>
        <br>
        <div class="row">
          
          <div class="col-sm-3">
            <label>Leaving From</label>
          </div>
          <div class="col-sm-3">
            <label>Going to</label>
          </div>
          <div class="col-sm-3">
            <label>Departure Date</label>
          </div>
          <div class="col-sm-3 return_date">
            <label class="yet">Return Date</label>
          </div>
        </div>
        <div class="row">
          
          <div class="col-sm-3">
            <input type="text" id="from" class="form-control" placeholder="Source">
            <input type="hidden" id="from_id" name="place_from">
            <div style="border: 1px solid;border-color: #F5F5EB;width: 87%;z-index:11;position: absolute;background-color: white;">
              <ul id="myUL" style="list-style-type:none;margin-top: 1%;padding-left: 6%;display: none;">
              </ul>
            </div>
            </div>
          <div class="col-sm-3">
            <input type="text" id="to" class="form-control" placeholder="Desination">
            <input type="hidden" id="to_id" name="place_to">
            <div style="border: 1px solid;border-color: #F5F5EB;width: 87%;z-index:11;position: absolute;background-color: white;">
              <ul id="myUL2" style="list-style-type:none;margin-top: 1%;padding-left: 6%;display: none;">
              </ul>
            </div>
          </div>
          <div class='col-sm-3'>
            <input name="depart_date" id="godate" type="text" class="form-control required" required>
            <script>
              $("#godate").datepicker({
              format: 'dd-mm-yyyy',
              startDate: '-3d'
                  });
            </script> 
          </div>
          <div class='col-sm-3 return_date yet'>
            <input name="return_date" id="comedate" type="text" class="form-control required">
             <script>
              $("#comedate").datepicker({
              format: 'dd-mm-yyyy',
              startDate: '-3d'
                  });
            </script> 
         
          </div>
        </div>
        <br>
        <div class="row">
          
          <div class="col-sm-3">
            <label>Adult (12+ Yrs)</label>
          </div>
          <div class="col-sm-3">
            <label>Child (2-11 Yrs)</label>
          </div>
          <div class="col-sm-3">
            <label>Infant (0-2 Yrs)</label>
          </div>
        </div>
        <div class="row">
          
          <div class="col-sm-3"><input name="adult" type="text" class="form-control required number" required></div>
          <div class="col-sm-3"><input name="child" type="text" class="form-control number" ></div>
          <div class="col-sm-3"><input name="infant" type="text" class="form-control number" ></div>
          <div class="col-sm-3">
          <button type="submit" class="btn btn-warning search_button">Search</button>
          </div>
        </div>
        </form>
        <br>
      </div>
      <div class="row">
        <div class="col-sm-12 mainadimage">
          <img id="mainimage" src="<?php echo HTTP_ROOT . 'img/Ads/' . $ads['0']['Advertisement']['image']; ?>">
        </div>
      </div>
    </section>
<!--////////////////////////////////////////////////////////////////////////////////////////////////-->
      <!-- <div class="row">
        <div class="col-sm-10 ads">
          <div id="myCarousel" class="carousel slide" data-ride="carousel"  >
            <div class="carousel-inner" style="height:426px;">
              <div class="item active" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad44.jpg" alt="Ad" width="91.5%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad55.jpg" alt="Ad" width="91.5%">
              </div>

              <div class="item" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad55.jpg" alt="Ad" width="91.5%">
              </div>
            
              <div class="item" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad66.jpg" alt="Ad" width="91.5%">
              </div>
            </div>
          </div>
        </div>
      </div> -->
<!--////////////////////////////////////////////////////////////////////////////////////////////////-->
    <Section class="main_slider" style="background-color: white; opacity: 0.80;">
    <div>
      <div class="row" style="padding-top:2%; padding-bottom:2%; padding-left:2%">
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/bestprice.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:5%;">
                <b>Best Price Guranteed</b><br>
                Find out lowest price to
                destinations worldwide 
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/easybook.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:5%; padding-left:11%;">
                <b>Easy Booking</b><br>
                Search, select and save the
                fastest way to book your trip
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/customer.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:4%; padding-left:11%;">
                <b>24/7 Customer Care</b><br>
                <?php echo $content['Content']['content'];?> <br>
                +91 - <?php echo $content1['Content']['content'];?> | <?php echo $content2['Content']['content'];?>  
          </div>
        </div>
      </div>
    </div>
</section>
<?php }?>

</section>
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
  </script>
<script type="text/javascript">
  $('#search-form').validate();
</script>
<style type="text/css">
          .yet
          {
            display: none;
          }
        </style>
<script>

    $(document).ready(function(){
        $("#OneWay").click(function(){
            $(".yet").hide();
        });
        $("#RoundTrip").click(function(){
            $(".yet").show();
        });
    });

    //****************************************************
    
      $(document).ready(function(){
      baseUrl = 'http://'+$(location).attr('hostname')+'/Airfare/';
        $( "#from" ).keyup(function() {
          var letter = document.getElementById("from").value;
          if(letter == '')
          {
            $('#myUL').hide();
          }
          if (letter != '') {
                    $.ajax({
                        url: baseUrl + 'Home/search_box/' + letter,
                        success: function(resp) {
                            console.log(resp);
                            $("#myUL").show();
                            $("#myUL").html(resp);
                        }
                    });
                } 
          else 
          {
            $('#myUL').children().remove();
          }
        });
    });
     $(document).ready(function(){
      baseUrl = 'http://'+$(location).attr('hostname')+'/Airfare/';
        $( "#to" ).keyup(function() {
          var letter = document.getElementById("to").value;
          if(letter == '')
          {
            $('#myUL2').hide();
          }
          if (letter != '') {
                    $.ajax({
                        url: baseUrl + 'Home/search_box2/' + letter,
                        success: function(resp) {
                            console.log(resp);
                            $("#myUL2").show();
                            $("#myUL2").html(resp);
                        }
                    });
                } 
          else 
          {
            $('#myUL2').children().remove();
          }
        });
    });
   
  //********************************************************    
  //Validate the Dates
  $(document).ready(function(){
    $( "#comedate" ).change(function() {
       var Godate = $( "#godate" ).val();
       var Comedate = $( "#comedate" ).val();
       if(Godate > Comedate){
        alert('Return Date cannot be less than Departure Date!');
        document.getElementById("comedate").value = ""; 
       }
       
      });
    $( "#godate" ).change(function() {
       var Godate = $( "#godate" ).val();
       var Comedate = $( "#comedate" ).val();
       if(Comedate != ''){
       if(Godate > Comedate){
        alert('Return Date cannot be less than Departure Date!');
        document.getElementById("comedate").value = ""; 
       }
       }
      });
    });
</script>
<script type="text/javascript">
  function fn(name,id)
  {
    console.log(name);console.log(id);
    $('#from').val(name);
    $('#from_id').val(id);
    $('#myUL').hide();
  };
  function fs(name,id)
  {
    console.log(name);console.log(id);
    $('#to').val(name);
    $('#to_id').val(id);
    $('#myUL2').hide();
  };
</script>