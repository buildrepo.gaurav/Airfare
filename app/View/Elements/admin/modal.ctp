<!-- Apps Modal --> 
<!-- import modal -->
<div class="modal fade" id="msgmodal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Message</h3>
                        </div>
                        <div class="block-content">
                          <div class="row">
                            <div class="col-lg-offset-1 col-lg-10">
                              <p id="success-msg-show"></p>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        
<!-- end import modal -->
<!-- Opens from the button in the header -->
<div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-sm modal-dialog modal-dialog-top">
    <div class="modal-content"> 
      <!-- Apps Block -->
      <div class="block block-themed block-transparent">
        <div class="block-header bg-primary-dark">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title">Apps</h3>
        </div>
        <div class="block-content">
          <div class="row text-center">
            <div class="col-xs-6"> <a class="block block-rounded" href="index.html">
              <div class="block-content text-white bg-default"> <i class="si si-speedometer fa-2x"></i>
                <div class="font-w600 push-15-t push-15">Backend</div>
              </div>
              </a> </div>
            <div class="col-xs-6"> <a class="block block-rounded" href="frontend_home.html">
              <div class="block-content text-white bg-modern"> <i class="si si-rocket fa-2x"></i>
                <div class="font-w600 push-15-t push-15">Frontend</div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
      <!-- END Apps Block --> 
    </div>
  </div>
</div>
<!-- END Apps Modal --> 


  <!-- import modal -->
<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <form method="post" id="cform1" action="<?php echo HTTP_ROOT.'Users/addcustomer1/0'?>">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-primary-dark">
                          <!-- <h2>Add Customer</h2> -->
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Add Customer</h3>
                        </div>
                        <div class="block-content">
                          <div class="row">
                            <div class="col-lg-offset-1 col-lg-10">
                              
                                <div class="col-md-6">
                                  <!-- <div class="form-group">
                                    <label class="control-label" for="val-customer-code">Customer Code<span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="val-customer-code" name="val-customer-code" placeholder="">
                                  </div> -->
                                  <!-- <div class="form-group">
                                    <label class="control-label" for="val-customer-name">Customer Name<span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="val-customer-name" name="val-customer-name" placeholder="">
                                  </div> -->
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-address">Address<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-address" name="data[Contact][address1]" placeholder="Line1" >
                                  </div>
                                  <div class="form-group">
                                    <input class="form-control" type="text" id="val-customer-address2" name="data[Contact][address2]" placeholder="Line2" >
                                  </div>
                                  <div class="form-group">
                                    <input class="form-control" type="text" id="val-customer-address3" name="data[Contact][address3]" placeholder="Line3" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-address">City<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-city" name="data[Contact][city]" placeholder="City" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-address">State<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-state" name="data[Contact][state]" placeholder="State" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-address">Country<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-country" name="data[Contact][country]" placeholder="Country" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-postcode">Postal code<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-postcode" name="data[Contact][zipcode]" placeholder="" >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  
                                  <div class="row">
                                    <div class="form-group col-md-12">
                                      <label class="control-label" for="val-customer-contact-fname">Name<span class="text-danger">*</span></label>
                                      <input class="form-control required" type="text" id="val-customer-contact-fname" name="data[Contact][first_name]" placeholder="" >
                                    </div>
                                    <!-- <div class="form-group col-md-6">
                                      <label class="control-label" for="val-customer-contact-lname">Lastname<span class="text-danger">*</span></label>
                                      <input class="form-control required" type="text" id="val-customer-contact-lname" name="data[Contact][last_name]" placeholder="" value="<?php echo $contacts['Contact']['last_name'];?>">
                                    </div> -->
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-telephone">Telephone</label>
                                    <input class="form-control" type="text" id="val-customer-telephone" name="data[Contact][telephone]" placeholder="" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-mobile">Mobile<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-mobile" name="data[Contact][mobile]" placeholder="" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-fax">Fax</label>
                                    <input class="form-control" type="text" id="val-customer-fax" name="data[Contact][fax]" placeholder="" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-email">E-Mail<span class="text-danger">*</span></label>
                                    <input class="form-control required" type="text" id="val-customer-email" name="data[Contact][email]" placeholder="" required >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label" for="val-customer-web">Web</label>
                                    <input class="form-control" type="text" id="val-customer-web" name="data[Contact][web]" placeholder="" >
                                  </div>
                                </div>
                              
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-sm btn-primary" type="submit">Save</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        
        
<!-- end import modal -->
<style>
.modal-dialog.modal-lg {
    border: 1px solid;
}
.modal-backdrop.in
{
  opacity: 0;
}</style>