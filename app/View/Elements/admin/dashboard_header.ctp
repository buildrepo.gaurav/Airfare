  <!-- Sidebar -->
  <nav id="sidebar"> 
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll"> 
      <!-- Sidebar Content --> 
      <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
      <div class="sidebar-content"> 
        <!-- Side Header -->
        <div class="side-header side-content bg-white-op"> 
          <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
          <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close"> <i class="fa fa-times"></i> </button>
          <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
         
          <a class="h4 text-white" href="<?php echo HTTP_ROOT?>Users/dashboard"> <span class="font-w600">Airfare</span> <span class="sidebar-mini-hide"></span> </a> </div>
        <!-- END Side Header --> 
        
        <!-- Side Content -->
        <div class="side-content">
          <ul class="nav-main">
            <li> <a class="active" href="<?php echo HTTP_ROOT?>Users/dashboard"><i class="si si-grid"></i><span class="sidebar-mini-hide">Dashboard</span></a> </li>
            <li><a class="nav-submenu " data-toggle="nav-submenu" href="#"><i class="fa fa-plane" aria-hidden="true"></i><span class="sidebar-mini-hide">Flights</span></a>
              <ul>
                <li> <a href="<?php echo HTTP_ROOT?>Users/flightList"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Flight List</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/addFlight"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Add Flight</span></a> </li>
                <!-- <li> <a href="<?php echo HTTP_ROOT?>Users/addlocation"><i class="fa fa-male fa-female"></i><span class="sidebar-mini-hide">Add Location</span></a> </li> -->

              </ul>
            </li>
            <?php if($this->Session->read('Admin.type') == 0){?>       
            
            <li> <a href="<?php echo HTTP_ROOT?>Users/user"><i class="fa fa-user"></i><span class="sidebar-mini-hide">User List</span></a> </li>
            <!-- <li> <a href="<?php echo HTTP_ROOT?>Sales/SalesList"><i class="fa fa-user"></i><span class="sidebar-mini-hide">Sales Excecutive List</span></a> </li> -->

            

            <li><a class="nav-submenu " data-toggle="nav-submenu" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span class="sidebar-mini-hide">Manage Location</span></a>
              <ul>
                <li> <a href="<?php echo HTTP_ROOT?>Users/manageLocation"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Location List</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/addlocation"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Add Location</span></a> </li>

              </ul>
            </li>
            <li><a class="nav-submenu " data-toggle="nav-submenu" href="#"><i class="fa fa-bars" aria-hidden="true"></i><span class="sidebar-mini-hide">Master</span></a>
              <ul>

                <li> <a href="<?php echo HTTP_ROOT?>Users/content"><i class="fa fa-male fa-female"></i><span class="sidebar-mini-hide">Customer Support</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/adverts"><i class="fa fa-globe"></i><span class="sidebar-mini-hide">Advertisements</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/packages"><i class="fa fa-thumbs-o-up"></i><span class="sidebar-mini-hide">Special Packages</span></a> </li>

              </ul>
            </li>
            <li><a class="nav-submenu " data-toggle="nav-submenu" href="#"><i class="fa fa-list" aria-hidden="true"></i><span class="sidebar-mini-hide">Content Management</span></a>
              <ul>

                <li> <a href="<?php echo HTTP_ROOT?>Users/contactus"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Contact Us</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/aboutus"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">About Us</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/privacy"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">Privacy Policy</span></a> </li>
                <li> <a href="<?php echo HTTP_ROOT?>Users/tc"><i class="fa fa-circle"></i><span class="sidebar-mini-hide">T&C</span></a> </li>

              </ul>
            </li>
            <?php }?>
            <li> <a href="<?php echo HTTP_ROOT?>Users/bookings"><i class="fa fa-ticket"></i><span class="sidebar-mini-hide">Bookings</span></a> </li>
            
            <!--<li class="nav-main-heading"><span class="sidebar-mini-hide">Customized Add On</span></li>
            <li> <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-badge"></i><span class="sidebar-mini-hide">R&amp;D</span></a>
              <ul>
                <li> <a href="#">Create New Style</a> </li>
                <li> <a href="#">WIP Style</a> </li>
                <li> <a href="#">Manage Exiting Style</a> </li>
              </ul>
            </li>-->
            <li class="nav-main-heading"><span class="sidebar-mini-hide"></span></li>
            <li class="nav-main-heading"><span class="sidebar-mini-hide"></span></li>
            <li class="nav-main-heading"><span class="sidebar-mini-hide"></span></li>
            <li class="nav-main-heading"><span class="sidebar-mini-hide"></span></li>
            



          </ul>
        </div>
        <!-- END Side Content --> 
      </div>
      <!-- Sidebar Content --> 
    </div>
    <!-- END Sidebar Scroll Container --> 
  </nav>
  <!-- END Sidebar --> 
  
  <!-- Header -->
  <header id="header-navbar" class="content-mini content-mini-full"> 
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
      <li>
        <div class="btn-group">
          <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button" style="height:35px;padding-left: 19px;"> 
            <?php echo $profile['Admin']['username'];?>
            <span class="caret"></span> </button>
          
          <ul class="dropdown-menu dropdown-menu-right">
            <li class="dropdown-header">Profile</li>
           
            <li> <a tabindex="-1" href="<?php echo HTTP_ROOT?>Users/profile"> <i class="si si-user pull-right"></i>Profile </a> </li>
            <li> <a tabindex="-1" href="<?php echo HTTP_ROOT?>Users/changepassword"> <i class="si si-settings pull-right"></i>Change Password</a> </li>
            <li class="divider"></li>
            <li class="dropdown-header">Actions</li>
        <li> <a tabindex="-1" href="<?php echo HTTP_ROOT?>Users/logout"> <i class="si si-logout pull-right"></i>Log out </a> </li>
          </ul>
        </div>
      </li>
      
    </ul>
    <!-- END Header Navigation Right --> 

    <!-- END Header Navigation Center --> 
    <!-- <ul class="nav-header pull-right">
      <?php foreach($allwarename as $aware){?>
      <li>
        <div class="btn-group">
          <?php if($aware['Location']['id'] == $this->Session->read('Auth.User.location_id')){?>
          <a class="btn btn-success" title="Current warehouser"><?php echo $aware['Location']['name'];?></a>
          <?php } else{?>
          <a href="<?php echo HTTP_ROOT.'Users/change_warehouse/'.$aware['Location']['id']?>" class="btn btn-primary" title="Change warehouser"><?php echo $aware['Location']['name'];?></a>
          <?php }?>
          
        </div>
      </li>
      <?php }?>
    </ul> -->
    <!-- END Header Navigation Center --> 
    
    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
      <li class="hidden-md hidden-lg"> 
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button"> <i class="fa fa-navicon"></i> </button>
      </li>
      <li class="hidden-xs hidden-sm"> 
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button"> <i class="fa fa-ellipsis-v"></i> </button>
      </li>
      <!-- <li> 
        
        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#apps-modal" type="button"> <i class="si si-grid"></i> </button>
      </li>
      <li class="visible-xs"> 
        <button class="btn btn-default" data-toggle="class-toggle" data-target=".js-header-search" data-class="header-search-xs-visible" type="button"> <i class="fa fa-search"></i> </button>
      </li>
      <li class="js-header-search header-search">
        <form class="form-horizontal" action="base_pages_search.html" method="post">
          <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">
            <input class="form-control" type="text" id="base-material-text" name="base-material-text" placeholder="Search..">
            <span class="input-group-addon"><i class="si si-magnifier"></i></span> </div>
        </form>
      </li> -->
    </ul>
    <!-- END Header Navigation Left --> 
  </header>
  <!-- END Header --> 