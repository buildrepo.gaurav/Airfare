  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  .search_for
  {
    margin-top: 4%;
    margin-bottom: 2%;
  }
  .proper
  {
    text-align: right;
    padding-top: 1%;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">System Users</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Users</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Users</div>
              </div>
            </div>
            <br>
            <form action="" method="get">
              <div class="row search_for">
                <div class="col-md-2 proper"><b>Search:</b></div>
                <div class="col-md-3">
                  <input type="text" name="keyword" class="form-control" placeholder="Search User">
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-danger">Search</button>
                </div>
              </div>
            </form>   
              <div class="clearfix"></div>
            
            <table class="dataTable table-hover">
              <thead>
                        <tr role="row">
                            <th>SNo<i class="sorting_icon"></i></th>
                            <th>Email<i class="sorting_icon"></i></th>
                            <th>Name<i class="sorting_icon"></i></th>
                            <th>Phone<i class="sorting_icon"></i></th>
                            <th>Status<i class="sorting_icon"></i></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php if(!empty($user_list)){ $i=1; foreach($user_list as $user){?>
                        <tr class="odd">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $user['User']['email']?></td>

                            <td><?php echo $user['User']['name']?></td>

                            <td><?php echo $user['User']['phone']?></td>

                            <?php if($user['User']['varify_status'] == 0){?>
                            <td>Inactive</td>
                            <?php } else{?>
                            <td>Active</td>
                            <?php  }?>
                            
                            <td>
                              <a title="View Orders" href="<?php echo HTTP_ROOT.'Users/userorders/'. base64_encode($user['User']['id']);?>"><i class="fa fa-eye" aria-hidden="true" style="color:green;"></i></a>
                              <a title="Update Status" href="<?php echo HTTP_ROOT.'Users/update_user/'.$user['User']['id']?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                              <a title="Delete" onclick="if(!confirm('Are you sure, you want to delete this User?')){return false;}" href="<?php echo HTTP_ROOT.'Users/deleteAll/User/'.base64_encode($user['User']['id'])?>"><i class="fa fa-trash" aria-hidden="true" style="color:red;"></i></a>
                            </td>
                        </tr>
                      <?php $i++; } } else {?>
                      <tr><td colspan="9"><b>No Records</b></td></tr>
                      <?php } ?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
