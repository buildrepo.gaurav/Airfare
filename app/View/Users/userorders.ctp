  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">User Details</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#orders">Orders</a></li>
        </ul>
        
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="orders">
            <div class="push">
              <div class="pull-left">
                
              </div>
              <div class="clearfix"></div>
            </div>
           <table class="dataTable table-hover">
              <thead>
                  <tr role="row">
                    <th>S.No.<i class="sorting_icon"></i></th>
                    <th>Order Number<i class="sorting_icon"></i></th>
                    <th>Type of Journey<i class="sorting_icon"></i></th>
                    <th>Source<i class="sorting_icon"></i></th>
                    <th>Flight<i class="sorting_icon"></i></th>
                    <th>Destination<i class="sorting_icon"></i></th>
                    <th>Return Flight<i class="sorting_icon"></i></th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Actions</th>
                  </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php $i=1; foreach($orders as $order){?>
                  <tr class="odd">
                      <td><?php echo $i;?></td>
                      <td><?php echo $order['Order']['order_number']?></td>
                      <td><?php echo $order['Order']['journey_type']?></td>
                      <td><?php echo $order['From']['location']?></td>
                      <td><?php echo $order['Flight']['flight_name']?></td>
                      <td><?php echo $order['To']['location']?></td>
                      <td><?php echo $order['ReturnFlight']['flight_name']?></td>
                      <td><?php echo $order['Order']['amount']?></td>
                      <?php if($order['Order']['status'] == 0){?>
                      <td>Failed</td>
                      <?php } else{?>
                      <td>Paid</td>
                      <?php }?>
                      <td><?php echo date("d-m-Y", strtotime($order['Order']['created_datetime']));?></td>
                      <td>
                        <a title="Delete" href="<?php echo HTTP_ROOT.'users/deleteAll/Order/'.base64_encode($order['Order']['id'])?>"><i class="fa fa-trash" aria-hidden="true" style="color:red;"></i></a>
                      </td>
                  </tr>
                <?php $i++; }?>
              </tbody>
            </table>

            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>   
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
