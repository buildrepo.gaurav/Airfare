   <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=crguy4zns9e84ndqnk63j545xy6q20rt2v15myzh0ylnodu6"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Content</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#specials">Airfare Specials</a></li>
          <li><a href="#holidays">Perfect Holidays</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="specials">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading"></div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div>
	           <table class="dataTable table-hover">
               <thead>
                 <th>SNo.</th>
                 <th>Image</th>
                 <th>Caption</th>
                 <th>Actions</th>
               </thead>
               <tbody>
                 <?php $i=1; foreach($specials as $special){?>
                 <tr>
                   <td><?php echo $i;?></td>
                   <td><img src="<?php echo HTTP_ROOT . 'img/Ads/' . $special['Package']['image'];?>"></td>
                   <td><?php echo $special['Package']['caption'];?></td>
                   <td><a href="<?php echo HTTP_ROOT .'Users/uploadspecials/' . base64_encode($special['Package']['id']);?>"><i class="fa fa-pencil"></i></a></td>
                 </tr>
                 <?php $i++; } ?>
               </tbody>
             </table>
        	 </div>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="clearfix"></div>
            </div>    
          </div>  

          <div class="tab-pane fade in" id="holidays">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading"></div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div>
              <table class="dataTable table-hover">
               <thead>
                 <th>SNo.</th>
                 <th>Image</th>
                 <th>Caption</th>
                 <th>Caption 2</th>
                 <th>Actions</th>
               </thead>
               <tbody>
                 <?php $i=1; foreach($holidays as $holiday){?>
                 <tr>
                   <td><?php echo $i;?></td>
                   <td><img src="<?php echo HTTP_ROOT . 'img/Ads/' . $holiday['Package']['image'];?>"></td>
                   <td><?php echo $holiday['Package']['caption'];?></td>
                   <td><?php echo $holiday['Package']['captionx'];?></td>
                   <td><a href="<?php echo HTTP_ROOT .'Users/uploadspecials/' . base64_encode($holiday['Package']['id']);?>"><i class="fa fa-pencil"></i></a></td>
                 </tr>
                 <?php $i++; } ?>
               </tbody>
             </table>
          </div>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
