  <style>
  .pagination > li > span {
    float: left;
  }
  table, th, td
  {
    text-align: center;
  }
  .search_for
  {
    margin-top: 4%;
    margin-bottom: 2%;
  }
  .proper
  {
    text-align: right;
    padding-top: 1%;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Flight List</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Flights</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Flights</div>
              </div>
            </div>
            <br>
            <form action="" method="get">
              <div class="row search_for">
                <div class="col-md-2 proper"><b>Search:</b></div>
                <div class="col-md-3">
                  <select class="form-control" name="from">
                    <option value="">Select Source</option>
                    <?php foreach($locs as $loc){ ?>
                    <option value="<?php echo $loc['Location']['id'];?>"><?php echo $loc['Location']['location'];?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <select class="form-control" name="to">
                    <option value="">Select Destination</option>
                    <?php foreach($locs as $loc){ ?>
                    <option value="<?php echo $loc['Location']['id'];?>"><?php echo $loc['Location']['location'];?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-danger">Search</button>
                </div>
              </div>
            </form>
              <div class="clearfix"></div>
            
            <table class="dataTable table-hover">
              <thead>
                        <tr role="row">
                            <th>S.No.<i class="sorting_icon"></i></th>
                            <th>Flight No.<i class="sorting_icon"></i></th>
                            <th>Flight Name<i class="sorting_icon"></i></th>
                            <th>Source<i class="sorting_icon"></i></th>
                            <th>DateTime(D)<i class="sorting_icon"></i></th>
                            <th>Destination<i class="sorting_icon"></i></th>
                            <th>DateTime(A)<i class="sorting_icon"></i></th>
                            <th>Status<i class="sorting_icon"></i></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                      <?php if(!empty($flight_list)){ $i=1; foreach($flight_list as $flight){?>
                        <tr class="odd">
                            <td><?php echo $i;?></td>
                            <td><?php echo $flight['FlightDetail']['flight_no']?></td>

                            <td><?php echo $flight['FlightDetail']['flight_name']?></td>

                            <td><?php echo $flight['From']['location']?></td>
                            <td><?php echo date("d-m-Y H:i:s", strtotime($flight['FlightDetail']['datetime_departure']));?></td>
                            <td><?php echo $flight['To']['location']?></td>
                            <td><?php echo date("d-m-Y H:i:s", strtotime($flight['FlightDetail']['datetime_arrival']));?></td>

                            <?php if($flight['FlightDetail']['status'] == 0){?>
                            <td>Inactive</td>
                            <?php } else{?>
                            <td>Active</td>
                            <?php }?>
                            
                            <td>
                              <a title="Update Status" href="<?php echo HTTP_ROOT.'Users/update_flight_status/'.base64_encode($flight['FlightDetail']['id'])?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                              <a title="Edit" href="<?php echo HTTP_ROOT.'Users/addFlight/'.base64_encode($flight['FlightDetail']['id'])?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <a title="Delete" href="<?php echo HTTP_ROOT.'users/deleteAll/FlightDetail/'.base64_encode($flight['FlightDetail']['id'])?>" onclick="if(!confirm('Are you sure, you want to delete this Flight?')){return false;}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++; } } else {?>
                      <tr><td colspan="9"><b>No Records</b></td></tr>
                      <?php }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
