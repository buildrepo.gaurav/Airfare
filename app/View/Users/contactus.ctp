  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=crguy4zns9e84ndqnk63j545xy6q20rt2v15myzh0ylnodu6"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Contact Us</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">People</a></li>
          <li><a href="#content">Content</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">People</div>
              </div>
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                      <tr role="row">
                          <th>SNo.</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>GST</th>
                          <th>Message</th>
                          <th>Status</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $i=1; foreach($people as $tuff){?>
                        <tr class="odd">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $tuff['Contact']['name']; ?></td>
                            <td><?php echo $tuff['Contact']['email']; ?></td>
                            <td><?php echo $tuff['Contact']['mobile']; ?></td>
                            <td><?php echo $tuff['Contact']['gst']; ?></td>
                            <td><?php echo $tuff['Contact']['message']; ?></td>
                            <td><?php if($tuff['Contact']['status'] == 0) echo 'Pending'; else echo 'Responded'; ?>
                            <td>
                              <a title="Change Status" href="<?php echo HTTP_ROOT.'Users/changestatus/'.base64_encode($tuff['Contact']['id'])?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                              <a title="Delete" onclick="if(!confirm('Are you sure, you want to delete this Record?')){return false;}" href="<?php echo HTTP_ROOT.'Users/deleteAll/Contact/'.base64_encode($tuff['Contact']['id'])?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++; } ?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div> 
          <div class="tab-pane fade in" id="content">
            <form action="<?php echo HTTP_ROOT.'Users/contactaddress'?>" method="post">
                <textarea rows="20" style="margin-bottom: 2%;" class="form-control" name="content" placeholder="Content"><?php echo $data['address']['Content']['content'];?></textarea>
                <label>Fax</label>
                <input type="text" class="form-control" name="fax" value="<?php echo $data['fax']['Content']['content'];?>" placeholder="Fax"><br>
                <label>Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $data['email']['Content']['content'];?>"><br>
                <label>Enquiry mail</label>
                <input type="text" class="form-control" name="enquiry" placeholder="Enquiry mail" value="<?php echo $data['enquiry']['Content']['content'];?>">
                <button class="btn btn-primary" style="margin-top: 2%;    margin-bottom: 1%;">Save</button>
            </form>
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
