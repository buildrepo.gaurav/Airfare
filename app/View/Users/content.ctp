   <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=crguy4zns9e84ndqnk63j545xy6q20rt2v15myzh0ylnodu6"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">24/7 Customer Support Content</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Content</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Content</div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div>
	            <form id="customerform" action="" method="post">
	            	<div class="row">
	            		<div class="col-md-6">
			            	<label><b>Description</b></label><br>
			            	<input type="text" name="data[desc]" class="form-control required" placeholder="Description" value="<?php echo $content['Content']['content']?>">
			            	<label><b>Contact Number 2</b></label><br>
			            	<input type="text" name="data[cont2]" class="form-control" placeholder="Contact Number" value="<?php echo $content2['Content']['content']?>">
		            	</div>
		            	<div class="col-md-6">
			            	<label><b>Contact Number 1</b></label><br>
			            	<input type="text" name="data[cont1]" class="form-control required" placeholder="Contact Number" value="<?php echo $content1['Content']['content']?>">
			            	<button type="submit" class="btn btn-primary" style="margin-top: 4.5%;">Save</button>
		            	</div>
	            	</div>
	            </form>
        	</div>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
