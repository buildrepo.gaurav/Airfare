  <!-- Main Container -->
  <main id="main-container">
    <form class="form-horizontal" action="" method="post">
    <div class="content">
    <div class="pull-left">
      <div class="h2 page-heading">Edit Warehouse</div>
    </div>
       <div class="pull-right"> <!-- <a href="" data-toggle="modal" data-target="#modal-fadein" class="btn btn-primary">Import / Export Stock warehouse Data</a> --></div>
          <div class="clearfix"></div>
          <br>
          
          
       <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<!-- <li class="active"><a href="#summary">Summary</a></li> -->
            <li class="active"><a href="#add-location">Edit warehouse</a></li>
          
           
        </ul>
        <div class="block-content tab-content">
         
			
          <div class="tab-pane fade in active" id="add-location">
          
          	
            
            	<div class="form-group">
                
                	<label class="col-lg-4 control-label">Warehouse Name</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][name]" type="text" placeholder="Name" required value="<?php echo $location['Location']['name']?>">
                    <input name="data[Location][id]" type="hidden" id="warehouse_id" required value="<?php echo $location['Location']['id']?>">
                    </div>
                    
                
                </div>

                <div class="form-group">
                
                    <label class="col-lg-4 control-label">System User</label>
                    <div class="col-lg-8">
                    <a href="" data-toggle="modal" data-target="#usermodal" class="btn btn-primary">System User List</a>
                        
                        <!-- <option value="2">Employee</option>
                        <option value="3">Customer</option> -->
                    <!-- </select> -->
                    </div>
                    
                
                </div>

                <!-- <div class="form-group">
                
                    <label class="col-lg-4 control-label">Phone</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][phone]" type="text" placeholder="Phone" required>
                    </div>
                    
                
                </div>

                <div class="form-group">
                
                    <label class="col-lg-4 control-label">Email</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][email]" type="text" placeholder="Email" required>
                    </div>
                    
                
                </div> -->
                
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">Address Line1</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][address1]" type="text" placeholder="Address Line1" value="<?php echo $location['Location']['address1']?>">
                    </div>
                    
                
                </div>
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">Address Line2</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][address2]" type="text" placeholder="Address Line2" value="<?php echo $location['Location']['address2']?>">
                    </div>
                    
                
                </div>
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">Address Line3</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][address3]" type="text" placeholder="Address Line3" value="<?php echo $location['Location']['address3']?>">
                    </div>
                    
                
                </div>
                
                
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">City</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][city]" type="text" placeholder="City" value="<?php echo $location['Location']['city']?>">
                    </div>
                    
                
                </div>
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">State</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][state]" type="text" placeholder="State" value="<?php echo $location['Location']['state']?>">
                    </div>
                    
                
                </div>
                
                 <div class="form-group">
                
                	<label class="col-lg-4 control-label">Zip Code</label>
                    <div class="col-lg-8">
                    <input class="form-control" name="data[Location][zipcode]" type="text" placeholder="Zip Code" value="<?php echo $location['Location']['zipcode']?>">
                    </div>
                    
                
                </div>
                
                <div class="form-group">
                
                	<label class="col-lg-4 control-label">Country</label>
                    <div class="col-lg-8">
                    
                    <input class="form-control" name="data[Location][country]" type="text" placeholder="Country" value="<?php echo $location['Location']['country']?>">
                    </div>
                    
                
                </div>
                
                <div class="form-group">
                
                    <label class="col-lg-4 control-label">Type</label>
                    <div class="col-lg-8">
                    <?php if($location['Location']['type'] != 1){?>
                    <select class="form-control" id="" name="data[Location][type]" placeholder="Choose role" required>
                        <option>Select</option>
                        <option value="2" <?php echo @$location['Location']['type'] == 2 ? 'selected' : '';?>>Warehouse</option>
                        <option value="3" <?php echo @$location['Location']['type'] == 3 ? 'selected' : '';?>>Consignment Warehouse</option>
                    </select>
                    <?php } else{?>
                    <input type="hidden" name="data[Location][type]" value="<?php echo $location['Location']['type']?>">
                    <input class="form-control" type="text" value="Main Warehouse" readonly>
                    <?php }?>
                    </div>
                    
                
                </div>
                
               <!-- <div class="form-group">
                
                	
                    <div class="col-md-offset-4 col-md-8 checkbox">
                    <label>
                    <input type="checkbox" name="data[Location][hold_stock]">
                    Hold Stock</label>
                    </div>
                    
                
                </div> --> 
                
                <div class="form-group">
                
                	
                    <div class="col-md-offset-4 col-md-8">
                   	<button type="submit" class="btn btn-primary">Update Location</button>
                    </div>
                    
                
                </div> 
               
          
          </div>
           
          
          </div>
        </div>
     
    </div>

    <div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">System User (Select User)</h3>
                        </div>
                        <div class="block-content">
                          <div class="row">
                            <div class="col-lg-offset-1 col-lg-10">
                                <!-- <div class="form-group">
                                  
                                    <?php foreach($users as $user){?>
                                    <div class="col-md-3">
                                    <input type="checkbox" class="user_id" name="user_id[]" value="<?php echo $user['User']['id']?>" <?php echo in_array($user['User']['id'], $userlist) ? 'checked' : '';?>> <?php echo $user['User']['name']?>
                                    </div>
                                    <?php }?>
                                </div> -->
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th>System User</th>
                                            <th>Access</th>
                                            <th>Approve</th>
                                            <th>Packed/Shipped</th>
                                            <th>Received/Reject</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach($userlist as $user){?>
                                        <tr>
                                            <td>
                                                <?php echo $user['User']['name']?>
                                                <input type="hidden" name="data[UserW][<?php echo $i;?>][UserWarehouse][user_id]" value="<?php echo $user['User']['id']?>">
                                                <input type="hidden" name="data[UserW][<?php echo $i;?>][UserWarehouse][id]" value="<?php echo $user['UserWarehouse']['id']?>">
                                                <?php if(empty($user['UserWarehouse']['warehouse_id'])){ $warehouse_id = $location['Location']['id']; } else{ $warehouse_id = $user['UserWarehouse']['warehouse_id']; }?>
                                                <input type="hidden" name="data[UserW][<?php echo $i;?>][UserWarehouse][warehouse_id]" value="<?php echo $warehouse_id?>">
                                            </td>
                                            <td><input type="checkbox" name="data[UserW][<?php echo $i;?>][UserWarehouse][access]" value="1" <?php echo $user['UserWarehouse']['access'] == 1 ? 'checked' : '';?>></td>
                                            <td><input type="checkbox" name="data[UserW][<?php echo $i;?>][UserWarehouse][approve]" value="1" <?php echo $user['UserWarehouse']['approve'] == 1 ? 'checked' : '';?>></td>
                                            <td><input type="checkbox" name="data[UserW][<?php echo $i;?>][UserWarehouse][packed]" value="1" <?php echo $user['UserWarehouse']['packed'] == 1 ? 'checked' : '';?>></td>
                                            <td><input type="checkbox" name="data[UserW][<?php echo $i;?>][UserWarehouse][received]" value="1" <?php echo $user['UserWarehouse']['received'] == 1 ? 'checked' : '';?>></td>
                                        </tr>
                                        <?php $i++; }?>
                                </table>
                            </div>

                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
  </main>
  <!-- END Main Container --> 
  <script>
$(document).ready(function(){
    $('.user_id').on('change',function(){
      var user_id = $(this).val();
      var warehouse_id = $('#warehouse_id').val();
      //console.log(check_val);
      //console.log(role_id);
      if($(this).is(':checked') == false){
        //console.log('1');
        if(user_id != ""){
          //console.log('2');
          $.ajax({
                  type : 'post',
                  data : {user_id : user_id, warehouse_id : warehouse_id},
                  url : '<?php echo HTTP_ROOT?>Users/delete_user_warehouse',
                  success : function(response){
                    console.log(response);
                        
                    }
              })
        }
      }
    });
    $('#usermodal').modal('show');
});
</script>