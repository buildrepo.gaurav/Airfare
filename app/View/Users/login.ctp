  <!-- Main Container -->
  <style>
    .border-line{
          width: 8%;
    background: #025192;
    display: inline-block;
    margin-bottom: 20px;
    height: 3px;
    }
    .row{
      width: 100%;
      margin-left: 0px;
    }
  </style>
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="text-center">
            <div class="h2 page-heading">Admin Login</div>
            <span class="border-line"></span>
          </div>
          
          <div class="clearfix"></div>
     	</div>
     
        
        <div class="row">
         <div class="col-md-4 col-md-offset-4">
           <div class="block block-bordered">       
            <div class="form-horizontal block-content">
                
               
                      <form method="post">
                      <div class="row">
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">UserName/Email</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control" type="text" name="data[Admin][email]" value="" maxlength="255">
                          </div>
                          </div>
                      </div>
                      
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Password</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control" type="password" name="data[Admin][password]" value="" maxlength="50">
                          </div>
                         </div> 
                      </div>
                      
                      
                      <div class="form-group">
                          <div class=" col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                          </div>
                      </div>
                      </div>
                      </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 