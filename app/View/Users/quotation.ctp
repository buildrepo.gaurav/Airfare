  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content ">
      <div class="pull-left">
        <div class="h2 page-heading">Quotation</div>
      </div>
      <div class="pull-right"> <a href="<?php echo HTTP_ROOT?>Users/addquotation" class="btn btn-primary">Create New</a> <!-- <a href="#" class="btn btn-danger">Delete</a> --> </div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
         <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#quotations">Quotations</a></li>
            
        </ul>
        <div class="block-content tab-content">
         
			<div class="tab-pane fade in active" id="quotations">
          <table id="productlist1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>S.No.</th>
                <th>Quotation No.</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach($quotations as $quotation){?>
              <tr>
                <td><?php echo $i;?></td>
                <td><a><?php echo $quotation['Quotation']['quotation_no']?></a></td>
                <td><a><?php echo $quotation['Contact']['first_name']?></a></td>
                <td><?php echo $quotation['Quotation']['date']?></td>
                <td><?php echo $quotation['Quotation']['grand_total']?>/-</td>
                <td>
                  <?php if($quotation['Quotation']['status'] == 1){?>
                  <span class="badge h5 text-white badge-success">Approve</span>
                  <?php } else if($quotation['Quotation']['status'] == 2){?>
                  <span class="badge h5 text-white badge-danger">Reject</span>
                  <?php } else{?>
                  <span class="badge h5 text-white badge-warning">Pending</span>
                  <?php }?>
                </td>
                <td>
                  <a href="<?php echo HTTP_ROOT.'Users/view_quotation/'.$quotation['Quotation']['id']?>" class="btn btn-primary">View</a>
                  <?php if($quotation['Quotation']['invoice_status'] == 0 && $quotation['Quotation']['status'] == 1){?>
                  <a href="<?php echo HTTP_ROOT.'Users/generate_invoice/'.$quotation['Quotation']['id']?>" class="btn btn-primary">Generate Invoice</a>
                  <?php }?>
                </td>
              </tr>
              <?php $i++; }?>
              <!-- <tr>
                <td><input type="checkbox"></td>
                <td><a href="quotation-detail.html">DGI00001</a></td>
                <td><a href="quotation-detail.html">Acme Supplier</a></td>
                <td>21-6-2016</td>
                <td>25-6-2016</td>
                <td>3000/-</td>
                <td><span class="badge h5 text-white badge-danger">Expired</span></td>
              </tr> -->
            </tbody>
          </table>
          	</div>
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
  </main>
  <!-- END Main Container --> 
  <script src="<?php echo HTTP_ROOT?>js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo HTTP_ROOT?>js/plugins/datatables/dataTables.bootstrap.min.js"></script>  
<script>
  $(function () {
   
    $('#productlist1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>