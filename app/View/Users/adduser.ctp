  <style>
  #ware{
    display: none;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
            <div class="h2 page-heading">New User</div>
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <form method="post" enctype="multipart/form-data">
        <div class="block block-bordered">
            <div class="form-horizontal block-content">
                <!-- <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Role&nbsp;*</label>
                </div>
                <div class="col-md-8">
                <select class="form-control">
                    <option value="4">Seller Full Access</option>
                    <option value="6">Seller Catalog Access</option>
                    <option value="7">Seller Order Access</option>
                    <option value="9">Seller API Access</option>
                    <option value="14">Seller API Product Access</option>
                    <option value="15">Seller API Order Access</option>
                </select>
                </div>
                </div> -->
                
                <!-- <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Email Address&nbsp;*</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="email" value="" maxlength="255">
                </div></div> -->
                
                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Name</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[User][name]" id="name" value="" maxlength="50" required>
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Email</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="email" name="data[User][email]" id="email" value="" maxlength="50" required>
                </div></div>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Password</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="password" name="data[User][password]" id="name" value="" maxlength="50" required>
                </div></div>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Phone</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[User][phone]" id="name" value="" maxlength="50" required>
                </div></div>

                <!-- <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Date of Birth</label>
                </div>
                <div class="col-md-8">
                    <div class="input-group date">
                        <input class="form-control datepicker" type="text" id="val-date-available" name="data[User][dob]" placeholder="Choose a date.." required>
                        <span class="input-group-addon"> </div>
                </div></div> -->

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Role</label>
                </div>
                <div class="col-md-8">
                <select class="form-control" name="data[User][role_id]" placeholder="Choose role" required>
                    <option value="">Select</option>
                    <?php foreach($roles as $role){?>
                    <option value="<?php echo $role['Role']['id']?>"><?php echo $role['Role']['role_name']?></option>
                    <?php }?>
                    <!-- <option value="2">Employee</option>
                    <option value="3">Customer</option> -->
                  </select>
                </div></div>

                <!-- <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Warehouse</label>
                </div>
                <div class="col-md-8">
                <select class="form-control" id="" name="warehouse[]" placeholder="Choose role" multiple>
                    <option selected>Select</option>
                    <?php foreach($locations as $location){?>
                    <option value="<?php echo $location['Location']['id']?>"><?php echo $location['Location']['name']?></option>
                    <?php }?>
                  </select>
                  Note : Press ctrl then select
                </div> </div> -->

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Profile</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="file" name="DishImage" id="">
                </div></div>
                
                <!-- <div class="form-group">
                    <div class="col-md-4">
                    	<label class="control-label">Default Language&nbsp;*</label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control">
                            <option value="3">Malay</option>
                            <option value="1">English</option>
                        </select>
                    </div>
                </div> -->
                
                <!-- <div class="form-group">
                    <div class="col-md-4">
                    	<label class="control-label">Active&nbsp;*</label>
                    </div>
                    <div class="col-md-8">
                    	<input type="checkbox" name="active" id="active" value="1" checked="checked">
                    </div>
                </div> -->
                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 
  <script>

$(document).ready(function () {
    
    // $('.dataTables_filter').find('input').addClass('.form-control');

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#role').on('change', function() {
        var role = $('#role').val();
        if(role == 1){
            $("#ware").css("display", "block");
        }
        else if(role == 0){
            $("#ware").css("display", "block");
        }
        else{
            $("#ware").css("display", "none");    
        }
        
    });
});
</script>