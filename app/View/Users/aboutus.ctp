  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=crguy4zns9e84ndqnk63j545xy6q20rt2v15myzh0ylnodu6"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">About Us</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Content</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Content</div>
              </div>
                    <!-- <div class="pull-right">
                    <a href="<?php echo HTTP_ROOT?>Users/adduser" class="btn btn-default"><i class="fa fa-plus"></i> Add User</a>
                    </div> -->
              <div class="clearfix"></div>
            </div>
            <form action="" method="post">
                
                <textarea rows="20" style="margin-bottom: 2%;" class="form-control" name="content" placeholder="Content"><?php echo $content['Content']['content']; ?></textarea>
                <button class="btn btn-primary" style="margin-top: 2%;">Save</button>
            </form>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
