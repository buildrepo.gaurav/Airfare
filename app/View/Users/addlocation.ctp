<main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Locations</div>
      </div>
      <div class="pull-right"><a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#contact-details">Add Locations</a> </li>

        </ul>
        <div class="block-content tab-content" style="padding: 3%;">
          <div class="row">
            <label><b>Name</b><span style="color:red;">*</span></label>
            <input type="text" id="loc" class="form-control required" name="data[Location][location]" value="<?php echo @$location['Location']['location']?>">
            <input type="hidden" class="form-control" name="data[Location][id]" value="<?php echo @$location['Location']['id']?>">
            <br>
            <label><b>Short Code</b><span style="color:red;">*</span></label>
            <input type="text" id="codee" class="form-control required" name="data[Location][code]" value="<?php echo @$location['Location']['code']?>">
            <br>
            <button style="float: right;" type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>
    </form>
  </main>
  <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
  <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
  <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
  <script>
    $('#cform').validate();
  </script>