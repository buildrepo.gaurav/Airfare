  <style>
  #ware{
    display: none;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
            <div class="h2 page-heading">Change Password</div>
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <form method="post" enctype="multipart/form-data" id="change-password">
        <div class="block block-bordered">
            <div class="form-horizontal block-content">

                
                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Current Password</label>
                </div>
                <div class="col-md-8">
                <input class="form-control required" type="password" name="oldpass" id="oldpass" value="" maxlength="50" required>
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">New Password</label>
                </div>
                <div class="col-md-8">
                <input class="form-control required" type="password" name="data[Admin][password]" id="newpass" value="" maxlength="50" required>
                </div></div>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Confirm Password</label>
                </div>
                <div class="col-md-8">
                <input class="form-control required" type="password" name="cpass" id="cpass" value="" maxlength="50" required>
                </div></div>

                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?php echo HTTP_ROOT?>Users/dashboard" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 
  <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
  <script>

$(document).ready(function () {
    
baseUrl = 'http://'+$(location).attr('hostname')+'/Airfare/';




    $('#change-password').validate({
        rules: {
                oldpass: {
                    required: true,
                   remote: {
                            url: baseUrl+"Users/check_password",
                            type: "post",
                            
                         }
                }        
              },
        messages : {
                    oldpass: {
                        remote: "Please enter correct Current Password."
                    }
                
                }     
    });
    $("#cpass").rules('add',{equalTo: "#newpass",
    messages: {equalTo: "New password and confirm password field doesn't match."}});
});
</script>