<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Advertisements</div>
      </div>
      <div class="pull-right"><a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#main">Main Image</a> </li>
           <li> <a href="#discount">Discounts</a> </li>
           <li> <a href="#end">End Page</a> </li>
        </ul>
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="main">
            <div>
                <div class="row">
                  <label><b>Current Image</b></label><br>
                  <img src="<?php echo HTTP_ROOT . 'img/Ads/' . $mainimage['Advertisement']['image'];?>" style="width: 100%;">
                </div>
                <div class="maindiv" style="margin-top: 2%;">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6" style="margin-bottom: 2%;">
                        <label><b>Edit Image</b></label><br>
                        <input type="file" class="form-control" name="image">
                        <input type="hidden" name="data[id]" value="1">
                      </div>
                      <div class="col-md-6" style="margin-bottom: 2%;padding-top: 2%;">
                        <input type="submit" class="btn btn-primary" value="Save">
                      </div>
                    </div>
                  </form>
                </div>
            </div>
          </div>
          <div class="tab-pane fade in" id="discount">
            <div>
                <div class="row">
                  <label><b>Current Image</b></label><br>
                  <img src="<?php echo HTTP_ROOT . 'img/Ads/' . $discounts['0']['Advertisement']['image'];?>" style="width: 30%;">
                </div>
                <div class="maindiv" style="margin-top: 2%;">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6" style="margin-bottom: 2%;">
                        <label><b>Edit Image</b></label><br>
                        <input type="file" class="form-control" name="image">
                        <input type="hidden" name="data[id]" value="2">
                      </div>
                      <div class="col-md-6" style="margin-bottom: 2%;padding-top: 2%;">
                        <input type="submit" class="btn btn-primary" value="Save"><a class="btn btn-default" href="<?php echo HTTP_ROOT . 'Users/setdefault/2';?>">Default</a>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row">
                  <label><b>Current Image</b></label><br>
                  <img src="<?php echo HTTP_ROOT . 'img/Ads/' . $discounts['1']['Advertisement']['image'];?>" style="width: 30%;">
                </div>
                <div class="maindiv" style="margin-top: 2%;">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6" style="margin-bottom: 2%;">
                        <label><b>Edit Image</b></label><br>
                        <input type="file" class="form-control" name="image">
                        <input type="hidden" name="data[id]" value="3">
                      </div>
                      <div class="col-md-6" style="margin-bottom: 2%;padding-top: 2%;">
                        <input type="submit" class="btn btn-primary" value="Save"><a class="btn btn-default" href="<?php echo HTTP_ROOT . 'Users/setdefault/3';?>">Default</a>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
          </div>
          <div class="tab-pane fade in" id="end">
            <div>
                <div class="row">
                  <label><b>Current Image</b></label><br>
                  <img src="<?php echo HTTP_ROOT . 'img/Ads/' . $end['Advertisement']['image'];?>" style="width: 100%;">
                </div>
                <div class="maindiv" style="margin-top: 2%;">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6" style="margin-bottom: 2%;">
                        <label><b>Edit Image</b></label><br>
                        <input type="file" class="form-control" name="image">
                        <input type="hidden" name="data[id]" value="4">
                      </div>
                      <div class="col-md-6" style="margin-bottom: 2%;padding-top: 2%;">
                        <input type="submit" class="btn btn-primary" value="Save">
                      </div>
                    </div>
                  </form>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    <!-- Page Content -->
    <div class="content"> </div>
    <!-- END Page Content -->

  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>