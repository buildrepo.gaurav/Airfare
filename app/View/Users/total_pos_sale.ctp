  <!-- Main Container -->
  <main id="main-container">
    <div class="content">
    <div class="pull-left">
      <div class="h2 page-heading">POS Sales</div>
    </div>
      <!-- <div class="pull-right"> <a href="new-transfer-process.html" class="btn btn-primary">New Transfer</a></div>-->
          <div class="clearfix"></div>
          <br>
          
          
       <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#transfer">POS Sales</a></li>
            <!-- <li><a href="#drawings">Drawings</a></li> -->
          
           
        </ul>
        <div class="block-content tab-content">
         
      <div class="tab-pane fade in active" id="transfer">
            
            
         <div class="pull-left"> 
            <form class="form-inline" action="" method="get">
                      <label>Search by : <a href="<?php echo HTTP_ROOT?>Users/pos_sale?cdate=today" class="btn btn-primary">Today</a> &nbsp;&nbsp;<b>OR</b>&nbsp;&nbsp; <input class="form-control datepicker" name="fdate" type="text" placeholder="From Date">
                        <input class="form-control datepicker" name="tdate" type="text" placeholder="To Date">
                        <!-- <input type="text" class="form-control" name="from" placeholder="From">
                        <input type="text" class="form-control" name="to" placeholder="To"> -->
                      
                        </label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="<?php echo HTTP_ROOT ?>Users/total_pos_sale/<?php echo $pos_id;?>" class="btn btn-primary">Reset</a>
                    </form>
                    </div>
                      <div class="pull-right"> </div>
          <div class="clearfix"></div>
          
          <br>

                    
              <table id="productlist1" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Bill Number</th>
                  <th>Date</th>
                  <th>Customer</th>
                  <th>Payment Method</th>
                  <th>Grand total</th>
                  <th>Payment Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach($possales as $possale){?>
                <tr>
                  <td><a href=""><?php echo $possale['PosSale']['bill_no']?></a></td>
                  <td><?php echo $possale['PosSale']['sale_date']?></td>
                  <td><?php echo $possale['Contact']['first_name']?></a></td>
                  <td>
                    <?php 
                    if($possale['PosSale']['payment_method'] == 1){
                      echo 'cash';
                    }
                    else{
                      echo 'Credit/Debit Card';
                    }
                    ?>
                  </td>
                  <td><?php echo $possale['PosSale']['grand_total']?></td>
                  
                  <td>
                    <?php if($possale['PosSale']['payment_status'] == 2){ ?>
                      <span class="badge h5 text-white badge-success"><?php echo 'Done';?></span>
                    <?php } else{ ?>
                      <span class="badge h5 text-white badge-warning"><?php echo 'Pending';?></span>
                      
                    <?php }?>
                  </td>
                  <td><a href="<?php echo HTTP_ROOT.'Users/edit_pos_sale/'.$possale['PosSale']['id']?>" class="btn btn-primary">Edit</a> <a href="<?php echo HTTP_ROOT.'Users/add_return/'.$possale['PosSale']['id'].'/pos';?>" class="btn btn-primary">Return</a></td>
                </tr>
                <?php 
                    if($possale['PosSale']['payment_method'] == 2){
                      @$cdamount = @$cdamount + $possale['PosSale']['grand_total']; 
                    }
                    else{
                      @$chamount = @$chamount + $possale['PosSale']['grand_total']; 
                    }
                    if(!empty($possale['PosSale']['return_amount'])){
                      @$return_amount = @$return_amount + $possale['PosSale']['return_amount'];
                    }
                } 
                @$tamount = @$chamount + @$cdamount; 
                foreach ($dailypossales as $value) {
                  @$init = @$init + $value['DailyPosSale']['initial_amount'];
                }
                $totcash = @$chamount + @$init;
                ?>

                
              </tbody>
              
            </table>
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td>Initial Amount</td>
                  <td><?php echo @$init?></td>
                </tr>
                <tr>
                  <td>Cash Amount</td>
                  <td><?php echo @$chamount;?></td>
                </tr>
                <tr>
                  <td>Card Amount</td>
                  <td><?php echo @$cdamount;?></td>
                </tr>
                <tr>
                  <td>Total Sale</td>
                  <td><?php echo @$tamount;?></td>
                </tr>
                <tr>
                  <td>Total Cash</td>
                  <td><?php echo @$totcash;?></td>
                </tr>
              </tbody>
            </table>
          
          </div>
           
          
          </div>
        </div>
     
    </div> 
  </main>
  <!-- END Main Container --> 
<script src="<?php echo HTTP_ROOT?>js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo HTTP_ROOT?>js/plugins/datatables/dataTables.bootstrap.min.js"></script>  
<script>
  $(function () {
   
    $('#productlist1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

$(document).ready(function() {
  $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
});
</script>