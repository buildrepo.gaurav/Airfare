  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <?php if($type == 0){?>
      <div class="h2 page-heading">Customers</div>
      <?php } else if($type == 1){?>
      <div class="h2 page-heading">Suppliers</div>
      <?php } else{?>
      <div class="h2 page-heading">Employees</div>
      <?php }?>
      <br>
        <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#summary">Summary</a></li>
            <!-- <li><a href="#email-listing">Email Listing</a></li> -->
           
          
           
        </ul>
        <div class="block-content tab-content">
         
			<div class="tab-pane fade in active" id="summary">
          <div class="pull-right"> <a href="<?php echo HTTP_ROOT.'Users/addcustomer/'.$type?>" class="btn btn-primary">Add New</a> <!-- <a href="#" class="btn btn-info">Edit</a> <a href="#" class="btn btn-danger">Delete</a> --> </div>
          <div class="clearfix"></div>
          <br>
          
            <table id="productlist1" class="dataTable table-hover">
                    <thead>
                        <tr role="row">
                            <th>Email<i class="sorting_icon"></i></th>
                            <th>Name<i class="sorting_icon"></i></th>
                            <th>Phone<i class="sorting_icon"></i></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php foreach($user_list as $user){?>
                        <tr class="odd">
                            <td><?php echo $user['Contact']['email']?></td>

                            <td><?php echo $user['Contact']['first_name']?></td>
                            <td><?php echo $user['Contact']['mobile']?></td>


                            <td><a href="<?php echo HTTP_ROOT.'Users/addcustomer/'.$type.'/'.$user['Contact']['id'].'/view';?>" class="btn btn-info">View</a> <a href="<?php echo HTTP_ROOT.'Users/addcustomer/'.$type.'/'.$user['Contact']['id']?>" class="btn btn-info">Edit</a> <a href="<?php echo HTTP_ROOT.'Users/detele_contact/'.$user['Contact']['id']?>" class="btn btn-danger">Delete</a></td></td>
                        </tr>
                      <?php }?>
                    </tbody>
        </table>
          </div>
          
          <div class="tab-pane fade in active" id="email-listing">
          
          </div>
          
          
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    <!-- Page Content -->
    <div class="content"> </div>
    <!-- END Page Content --> 
  </main>
  <!-- END Main Container --> 
  <script src="<?php echo HTTP_ROOT?>js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo HTTP_ROOT?>js/plugins/datatables/dataTables.bootstrap.min.js"></script>  
<script>
  $(function () {
   
    $('#productlist1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>