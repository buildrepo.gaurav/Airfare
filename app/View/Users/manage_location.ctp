 <style>
  .pagination > li > span {
    float: left;
  }
  .search_for
  {
    margin-top: 4%;
    margin-bottom: 2%;
  }
  .proper
  {
    text-align: right;
    padding-top: 1%;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">System Locations</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Locations</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Locations</div>
              </div>
            </div>
            <br>
            <form action="" method="get">
              <div class="row search_for">
                <div class="col-md-2 proper"><b>Search:</b></div>
                <div class="col-md-3">
                  <input type="text" name="keyword" class="form-control" placeholder="Search Location">
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-danger">Search</button>
                </div>
              </div>
            </form>   
              <div class="clearfix"></div>
            <table class="dataTable table-hover">
              <thead>
                <tr role="row">
                    <th>Location Name<i class="sorting_icon"></i></th>
                    <th>Location Code</th>
                    <th>Actions</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php if(!empty($locations)){ foreach($locations as $loc){?>
                <tr class="odd">
                    <td><?php echo $loc['Location']['location']?></td>
                    <td><?php echo $loc['Location']['code']?></td>
                    <td>
                      <a title="Edit" href="<?php echo HTTP_ROOT.'Users/addlocation/'.$loc['Location']['id']?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                      <a title="Delete" onclick="if(!confirm('Are you sure, you want to delete this Location?')){return false;}" href="<?php echo HTTP_ROOT.'Users/deleteAll/Location/'.base64_encode($loc['Location']['id'])?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php }} else {?>
                <tr><td colspan="9"><b>No Records</b></td></tr>
                <?php }?>
              </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
