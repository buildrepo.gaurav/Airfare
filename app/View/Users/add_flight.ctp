<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Flights</div>
      </div>
      <div class="pull-right"> <button type="submit" class="btn btn-primary">Save</button> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#contact-details">Flight Details</a> </li>

        </ul>
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="contact-details">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Flight No.<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-no" name="data[FlightDetail][flight_no]" placeholder="Flight No" value="<?php echo @$editFlight['FlightDetail']['flight_no']?>">
                  <input type="hidden" name="data[FlightDetail][id]" value="<?php echo @$editFlight['FlightDetail']['id']?>">
                </div>
                <div class="form-group">
                  <label class="control-label" >Flight Name<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-name" name="data[FlightDetail][flight_name]" placeholder="Flight Name" value="<?php echo @$editFlight['FlightDetail']['flight_name']?>">
                </div>
                <div class="form-group">
                  <label class="control-label " >Cost<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-cost" name="data[FlightDetail][cost]" placeholder="Cost" value="<?php echo @$editFlight['FlightDetail']['cost']?>">
                </div>
                <div class="form-group">
                  <label class="control-label " >Source<span class="text-danger">*</span></label>
                  
                  <select name="data[FlightDetail][source]" type="text" class="form-control required">
                    <option value=''>Select Source</option>
                    <?php foreach($locations as $location){?>
                      <option value="<?php echo $location['Location']['id']?>" <?php echo @$editFlight['FlightDetail']['source'] == $location['Location']['id'] ? "selected" : "";?>><?php echo $location['Location']['location']?></option>
                    <?php }?> 
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label" >DateTime Departure<span class="text-danger">*</span></label>
                  <div class='input-group date' id='datetimepicker2'>
                    <input class="form-control required" type="text" id="val-departure" name="data[FlightDetail][datetime_departure]" placeholder="YYYY-MM-DD HH:mm:ss" value="<?php echo @$editFlight['FlightDetail']['datetime_departure']?>">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar">
                        </span>
                      </span>
                    </div>
                </div>
                
                
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Email<span class="text-danger">*</span></label>
                  <input class="form-control required" type="email" id="val-email" name="data[FlightDetail][mail_id]" placeholder="Email" value="<?php echo @$editFlight['FlightDetail']['mail_id']?>">
                </div>
                <div class="form-group">
                  <label class="control-label" >Seat Avalibility<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-seat" name="data[FlightDetail][seat_availability]" placeholder="Seat Avalibility" value="<?php echo @$editFlight['FlightDetail']['seat_availability']?>">
                </div>
                <div class="form-group">
                  <label class="control-label " >Airfare Charges<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-cost" name="data[FlightDetail][airfare_charge]" placeholder="Airfare Charges" value="<?php echo @$editFlight['FlightDetail']['airfare_charge']?>">
                </div>
                <div class="form-group">
                  <label class="control-label" >Destination<span class="text-danger">*</span></label>
                  <select name="data[FlightDetail][destination]" type="text" class="form-control required">
                    <option value=''>Select Destination</option>
                    <?php foreach($locations as $location){?>
                      <option value="<?php echo $location['Location']['id']?>" <?php echo @$editFlight['FlightDetail']['destination'] == $location['Location']['id'] ? "selected" : "";?>><?php echo $location['Location']['location']?></option>
                    <?php }?> 
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label" >DateTime Arrival<span class="text-danger">*</span></label>
                  <div class='input-group date' id='datetimepicker1'>
                    <input class="form-control required" type="text" id="val-arrival" name="data[FlightDetail][datetime_arrival]" placeholder="YYYY-MM-DD HH:mm:ss" value="<?php echo @$editFlight['FlightDetail']['datetime_arrival']?>">
                    <span class="input-group-addon">
                      <span class="fa fa-calendar">
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>



        </div>
      </div>
    </div>
  </form>
    <!-- END Stats --> 
    
    <!-- Page Content -->
    <div class="content"> </div>
    <!-- END Page Content -->

  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
            $('#datetimepicker2').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>