<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Upload</div>
      </div>
      <div class="pull-right"><a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#contact-details">Edit</a> </li>

        </ul>
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="contact-details">
            <div class="maindiv">
              <form id="cform" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-12">
                    <label><b>Image</b></label>
                    <input type="file" class="form-control" name="image">
                  </div>
                  <div class="col-md-6" style="margin: 2%  0%">
                    <label><b>Caption</b></label>
                    <input type="text" class="form-control required" name="data[Package][caption]" value="<?php echo $one['Package']['caption'];?>">
                  </div>
                  <?php if(!empty($one['Package']['captionx'])){?>
                  <div class="col-md-6" style="margin: 2%  0%">
                    <label><b>Caption 2</b></label>
                    <input type="text" class="form-control required" name="data[Package][captionx]" value="<?php echo $one['Package']['captionx'];?>">
                  </div>
                  <?php }?>
                </div>
                <div class="row">
                  <div class="col-md-6" style="margin-bottom: 2%;">
                    <input type="submit" class="btn btn-primary" value="Save">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    <!-- Page Content -->
    <div class="content"> </div>
    <!-- END Page Content -->

  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>