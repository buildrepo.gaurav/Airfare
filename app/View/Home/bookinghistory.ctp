<style>
	body
	{
		background-color: #E6F2FF;
	}
	.container
	{
		background-color: white;
	}
	.listing1
	{
		margin-top: 3%;
	}
	.notice
	{
		margin-top: 2%;
	}
	table,th,td
	{
		text-align: center;
	}
	.fa:hover
	{
		color: blue!important;
	}
</style>
<div class="container">
	<div class="content">
		<div class="head">
			<h2>Booked History</h2>
			<span style="color:red; font-size: 11px;">*Only Last 50 Orders are accessible</span>
		</div>
		<div class="notice">
			<p><b>Note: </b>All the records older than last 50 transactions are erased and will not be accessible for reference.</p>
		</div>
		<div class="listing1">
			<table class="dataTable table-hover">
				
					<tr>
						<th>SNo.</th>
						<th>Order Number</th>
						<th>Journey Type</th>
						<th>Source</th>
						<th>Destination</th>
						<th>Date</th>
						<th>Return Date</th>
						<th>Flight No.</th>
						<th>Ret. Flight No.</th>
						<th>Amount</th>
						<th>Passengers</th>
						<th>View</th>
					</tr>
				
				<tbody>
					<?php if(count($find)) { ?>
					<?php $i=1; foreach($find as $found) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $found['Order']['order_number']; ?></td>
						<td><?php echo $found['Order']['journey_type'] ?></td>
						<td><?php echo $found['From']['location']; ?></td>
						<td><?php echo $found['To']['location']; ?></td>
						<td><?php echo $found['Order']['date']; ?></td>
						<?php if($found['Order']['journey_type'] == 'RoundTrip') { ?>
						<td><?php echo $found['Order']['return_date']; ?></td>
						<?php } else { ?>
						<td><?php echo ""; ?></td>
						<?php } ?>
						<td><?php echo $found['Flight_onward']['flight_no']; ?></td>
						<td><?php echo $found['Flight_return']['flight_no']; ?></td>
						<td><?php echo $found['Order']['amount']; ?></td>
						<td><?php echo $found['Order']['passengers']; ?></td>
						<td><a href="<?php echo HTTP_ROOT.'Home/invoice/'.base64_encode($found['Order']['id']); ?>"><i class="fa fa-th-list" aria-hidden="true"></i></a></td>
					<tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr><td colspan="11" style="text-align: center;">No Records Found</td></tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div> 