<section class="Offers">
<div><label id="heading_secton2">Airfare Specials</label></div>
	<div class="row">
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/Ads/<?php echo $specials['0']['Package']['image'];?>" alt="Lights" style="width:100%">
				<div class="caption">
				  <p><?php echo $specials['0']['Package']['caption'];?></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/Ads/<?php echo $specials['1']['Package']['image'];?>" alt="Lights" style="width:100%">
				<div class="caption">
				  <p><?php echo $specials['1']['Package']['caption'];?></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/Ads/<?php echo $specials['2']['Package']['image'];?>" alt="Lights" style="width:100%">
				<div class="caption">
				  <p><?php echo $specials['2']['Package']['caption'];?></p>
				</div>
			  </a>
			</div>
		</div>
	</div>
</section>

<section class="Holidays">
	<div><label id="heading_secton2">Perfect Holidays</label></div>
	<div class="row">
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero"><?php echo $holidays['0']['Package']['caption'];?></h2>
				   <p class="zero"><?php echo $holidays['0']['Package']['captionx'];?></p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT . 'img/Ads/' . $holidays['0']['Package']['image'];?>" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero"><?php echo $holidays['1']['Package']['caption'];?></h2>
				   <p class="zero"><?php echo $holidays['1']['Package']['captionx'];?></p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT . 'img/Ads/' . $holidays['1']['Package']['image'];?>" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero"><?php echo $holidays['2']['Package']['caption'];?></h2>
				   <p class="zero"><?php echo $holidays['2']['Package']['captionx'];?></p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT . 'img/Ads/' . $holidays['2']['Package']['image'];?>" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero"><?php echo $holidays['3']['Package']['caption'];?></h2>
				   <p class="zero"><?php echo $holidays['3']['Package']['captionx'];?></p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT . 'img/Ads/' . $holidays['3']['Package']['image'];?>" alt=""/></li>
			</ul>
		</div>
	</div>
	
</section>
<!--End Holiday Section-->
<section>
	<div class="parallax" style="background-image: url('<?php echo HTTP_ROOT ?>img/index/banner1.jpg');">
		<section class="advertise" style="margin-top:1%;padding-top: 4%;  margin-bottom:2%;">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<img src="<?php echo HTTP_ROOT . 'img/Ads/' . $ads['1']['Advertisement']['image'];?>" class="img-responsive advertisement2">
				</div>
				<div class="col-md-3">
					<img src="<?php echo HTTP_ROOT . 'img/Ads/' . $ads['2']['Advertisement']['image'];?>" class="img-responsive advertisement2">
				</div>
				<div class="col-md-2"></div>
			</div>
		</section>
	</div>
</section>

<!--End of parallax-->

<section class="Travel">
	<div><label id="heading_secton2">Travel Talks</label></div>
	<p style="margin-left:3%;">Sharing travel experiences - one story at a time</p>
	<div class="row">
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Organic_Meal.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				 <div class="caption">
				  <p>#GastroTraveller Picks out best 6 Restaurants in India<br><br><b>Read More</b></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Village_and_fields.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				<div class="caption">
				  <p>To Those Who Love The Mountains - Mystical Places to Go Off the Beaten Track In Uttrakhand<br><br><b>Read More</b></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Degraves_Street1.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				<div class="caption">
				  <p>A Foodie's Guide to the Best Restaurants in Melbourne<br><br><b>Read More</b></p>
				</div>
			 </a>
			</div>
		</div>
	
	</div>
</section>

<!--Advetise-->

<section class="advertise" style="margin-top:2%; margin-bottom:2%;">
	<div style="text-align: center;"><image class="advertisement" src="<?php echo HTTP_ROOT . 'img/Ads/' . $ads['3']['Advertisement']['image']; ?>"></div>
</section>
