
<div class="row">
	<div class="col-md-8 sections">
		<div class="head"><h3>Flight Details</h3></div>
		<div class="userdetails">
			<table width="70%">
				<tbody>
					<tr>
						<td><b>Journey:</b><td>
						<td><?php echo $order['journey_type']; ?></td>
						<!-- <td><b>Order Number:</b><td>
						<td><?php echo $order['order_number']; ?></td> -->
					</tr>
					<tr>
						<td><b>Source:</b><td>
						<td><?php echo $source['Location']['location']; ?></td>
						<td><b>Destination:</b><td>
						<td><?php echo $destination['Location']['location']; ?></td>
					</tr>
					<tr>
						<td><b>Date:</b><td>
						<td><?php echo $order['date'] ?></td>
						<?php if($order['journey_type'] == 'RoundTrip') { ?>
						<td><b>Return Date:</b><td>
						<td><?php echo $order['return_date'] ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td><b>Flight Name:</b><td>
						<td><?php echo $flight['FlightDetail']['flight_name']; ?></td>
						<td><b>Passengers:</b><td>
						<td><?php echo $order['passengers']; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box_detail">
			<div class="row">
				<div class="col-md-2 marg"><b>Flight No.</b></div>
				<div class="col-md-4 marg"><?php echo $flight['FlightDetail']['flight_no']; ?></div>
				<div class="col-md-2 marg"><b>Flight Name</b></div>
				<div class="col-md-4 marg"><?php echo $flight['FlightDetail']['flight_name']; ?></div>
				<br>
				<?php if($order['journey_type'] == 'RoundTrip') { ?>
				<div class="col-md-2 marg"><b>R. Flight No.</b></div>
				<div class="col-md-4 marg"><?php echo $flight_return['FlightDetail']['flight_no']; ?></div>
				<div class="col-md-2 marg"><b>R. Flight Name</b></div>
				<div class="col-md-4 marg"><?php echo $flight_return['FlightDetail']['flight_name']; ?></div>
				<br>
				<?php } ?>
				<div class="col-md-2 marg"><b>Source</b></div>
				<div class="col-md-4 marg"><?php echo $source['Location']['location']; ?></div>
				<div class="col-md-2 marg"><b>Destination</b></div>
				<div class="col-md-4 marg"><?php echo $destination['Location']['location']; ?></div>
				<br>
				<div class="col-md-2 marg"><b>Departure</b></div>
				<div class="col-md-4 marg"><?php echo $flight['FlightDetail']['datetime_departure']; ?></div>
				<div class="col-md-2 marg"><b>Arrival</b></div>
				<div class="col-md-4 marg"><?php echo $flight['FlightDetail']['datetime_arrival']; ?></div>
			</div>
		</div>
		<div class="box_detail">
			<div class="row">
				<h3 style="margin-left: 2%;">Passenger Details</h3>
				<form action="passengers/<?php echo base64_encode($order_id); ?>" method="post">
				<table width="90%" style="margin-left: 5%;margin-top: 3%;">
					<thead>
						<tr>
							<th>SNo.</th>
							<th>Name</th>
							<th>Age</th>
							<th>Gender</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; for(;$i<=$order['passengers'];) { ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><input type="text"  class="form-control" name="data[Pass][<?php echo $i;?>][Passenger][name]" required></td>
							<td><input type="text" class="form-control" name="data[Pass][<?php echo $i;?>][Passenger][age]" required></td>
							<td>
								<select class="form-control" name="data[Pass][<?php echo $i;?>][Passenger][gender]" required>
									<option>Male</option>
									<option>Female</option>
								</select>
							</td>
						</tr>
						<?php $i++; } ?>
					</tbody>
				</table>
				<!--</form>-->
			</div>
		</div>
	</div>
	<div class="col-md-3 sections">
		<div class="headb">
			<h3>Payment Details</h3>
		</div>
		<div class="row last">
			<div class="col-md-6">
				<b>Cost of Flight:</b>
			</div>
			<div class="col-md-6">
				<?php echo $flight['FlightDetail']['cost']; ?>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6" style="padding-left: 10%;">
					<b>Airfare Charges(Incl GST):</b>
				</div>
				<div class="col-md-6" style="padding-left: 7%;">
					<?php echo $flight['FlightDetail']['airfare_charge'] + $flight['FlightDetail']['gst_charge']; ?>
				</div>
			</div>
			<?php if($order['journey_type'] == 'RoundTrip') { ?>
			<br>
			<div class="col-md-6">
				<b>Cost of Return Flight:</b>
			</div>
			<div class="col-md-6">
				<?php echo $flight_return['FlightDetail']['cost']; ?>
			</div>
			<br>
			<?php } ?>

			<br>
			<div class="col-md-6">
				<b>No of Passengers:</b>
			</div>
			<div class="col-md-6">
				<?php echo $order['passengers']; ?>
			</div>
			<br>
			<div class="row">
				<?php if($user['User']['type'] == 2){?>
				<div class="col-md-6 shift"><b>Charges:</b></div>
				<div class="col-md-6"><input id="charge" type="number" name="charges" class="form-control" placeholder="Commission"></div>
				<?php } else {?>
				<div class="col-md-12"></div>
				<?php }?>
			</div>
			<br>
			<hr>
			<div class="col-md-12"></div>
			<div class="col-md-6">
				<h4>Total Amount</h4>
			</div>
			<div class="col-md-6">
				<h5 id="tot"><?php echo $order['amount']; ?></h5>
			</div>
			<br>
			<div class="col-md-12"></div>
			<div class="row shifting">
				<div class="col-md-6"><b>Use Wallet?</b></div>
				<div class="col-md-6"><input type="checkbox" id="usewallet" name=""></div>
			</div>
			<div class="row shifting balance">
				<div class="col-md-6">Wallet Balance:</div>
				<div class="col-md-6" style="color:red;"><?php echo $wallet;?>
				<input type="hidden" name="wallet" id="wall" value="0">	
				</div>
			</div>
			<hr>
			<button type="submit" class="btn btn-success payment">Pay Now</button>
		</div>
		</form>
		<input id="amount" type="hidden" name="amountt" value="<?php echo $order['amount']; ?>">
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#charge").keyup(function(){
	        var total = $('#amount').val();
	        total = parseInt(total);
	        if($('#charge').val() == "")
	        {
	        	tamount = total;
	        }
	        else
	        tamount = total + parseInt($('#charge').val());
	        console.log(tamount);
	        document.getElementById('tot').innerHTML = tamount;
	    });
	});
	$(document).ready(function(){
		$('#usewallet').change(function() {
			if($(this).is(":checked")) 
			{
	           $('.balance').show();
	           $('#wall').val(1);
	        }
	    });
	    $('#usewallet').change(function() {
			if($(this).is(":unchecked")) 
			{
	          $('.balance').hide();
	           $('#wall').val(0);
	        }
	    });
	});
</script>
<style>
	body
	{
		background-color: #E6F2FF;
	}
	.sections
	{
		margin-left: 2%;
		background-color: white;
		padding-bottom: 2%;

	}
	.head
	{
		padding-top: 5%;
	}
	.userdetails
	{
		margin-top: 5%;
	}
	.box_detail
	{
		margin-top: 3%;
		border: 1px solid;
		padding: 1%;
	}
	.marg
	{
		margin-top: 1%;
	}
	.headb
	{
		margin-top: 11%;
	}
	.last
	{
		margin-top: 11%;
	}
	.payment
	{
		width: 50%;
		margin-left: 28%;
	}
	.shifting
	{
		padding-left: 5%;
	}
	.balance
	{
		display:none;
	}
</style>