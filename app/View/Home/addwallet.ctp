<style type="text/css">
	body
	{
		background-color: #E6F2FF;
	}
	.panell
	{
		background-color: white;
		padding-right: 0%;
	}
	.paint
	{
		background-color: #001743;
		margin-bottom: 1%;
	}
	#tet
	{
		font-size: 157%;
	}
	.bodyy
	{
		min-height: 500px;
	}
	.paint2
	{
		padding: 2%;
		background-color: white;
	}
	.mess
	{
		color:red;
		display: none;
	}
</style>
<?php if(!empty(@$bal)){?>
<style type="text/css">.mess{display: block!important;}</style>
<?php }?>
<div class="row bodyy">
	<div class="col-md-1"></div>
	<div class="col-md-10 panell">
		<div style="padding: 1%;"><h2 style="font-size: 260%;">Wallet</h2></div>
		<div class="row paint">
			<div class="col-md-3" style="color: white;padding: 2%;"><span id="tet">Add to Wallet<i style="color:white;font-size: 150%;" class="fa fa-get-pocket" aria-hidden="true"></i></span></div>
			<div class="col-md-9" style="color: white;padding: 5%;"><span style="float:right;">Current Wallet Amount: Rs <?php echo $wallet; ?></span></div>
		</div>
		<span class="mess">*Please add the balance amount</span>
		<form action="" method="post" id="walletform">
		<div class="row paint2">
			<label style="margin-left: 1.5%;"><b>Enter Amount</b></label><br>
			<div class="col-md-5"><input type="text" class="form-control required number" value="<?php echo @$bal;?>" placeholder="Amount" name="amount" <?php if(!empty(@$bal)){echo "readonly";}?>></div>
			<div class="col-md-1"><button type="submit" class="btn btn-primary" style="margin-top: -1%;">ADD</button></div>
		</div>
		</form>
	</div>
	<div class="col-md-1"></div>
	
</div>
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
<script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
<script type="text/javascript">
	$('#walletform').validate();
</script>