<style>
	body
	{
		background-color: #E6F2FF;
	}
	.main
	{
		background-color: white;
	}
	.container 
	{
    	width: 908px!important;
	}
	.img-rounded {
    border-radius: 50%!important;
	}
	.pic
	{
		    text-align: center;
    		padding-top: 41%;
	}
	.details
	{
		margin-top: 9%;
	}
	.element
	{
		margin-top: 1%;
	}
	.file
	{
		margin-top: 8%;
		display: none;
	}
</style>
<div class="container main">
	<div class="row">
	<!--Profile Picture Section-->
	<form action="" method="post" enctype="multipart/form-data">
		<div class="col-md-4">
			<div class="pic">
				<img src="<?php echo HTTP_ROOT?>img/profile/<?php echo $data['User']['image']; ?>" class="img-rounded" alt="Profile Picture" style="width: 49%; height: 0%; border: 1px solid; padding: 1%;">
				<br>
				<div class="file">
					<input class="form-control" type="file" name="image" style="height: 5%;">
				</div>
			</div>
			<div style="text-align: center;">
				<a id="pic_edit" class="btn btn-primary" style="width: 27%; margin-top: 5%;">Edit</a> 
			</div>
		 	<br>
		 </div>
		<div class="col-md-7">
		<!--Profile Details section-->
			<div class="content">
				<h3>Edit Profile</h3>
				<br>
				
				<div class="details">
					<div class="row">
						<div class="col-md-4 element">
							Name: 
						</div>
						<div class="col-md-7 element">
							<input type="text" class="form-control" value="<?php echo $data['User']['name']; ?>" name="name">
						</div>
						<br>
						<div class="col-md-4 element">
							Email: 
						</div>
						<div class="col-md-7 element">
							<input type="text" class="form-control" value="<?php echo $data['User']['email']; ?>" name="email" disabled>
						</div>
						<br>
						<div class="col-md-4 element">
							Mobile: 
						</div>
						<div class="col-md-7 element">
							<input type="text" class="form-control" value="<?php echo $data['User']['phone']; ?>" name="phone">
						</div>
						<br>
						<div class="col-md-4 element">
							Gender: 
						</div>
						<div class="col-md-7 element">
							<label class="radio-inline"><input type="radio" value="Male" name="gender" <?php if($data['User']['gender'] == "Male") { ?> checked <?php } ?>>Male</label>
      						<label class="radio-inline"><input type="radio" value="Female" name="gender" <?php if($data['User']['gender'] == "Female") { ?> checked <?php } ?>>Female</label>
						</div>
						<br>
						<div class="col-md-4 element"></div>
						<div class="col-md-7 element">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		</form>
	</div>
</div>
<script>
        $(document).ready(function(){
            $("#pic_edit").click(function(){
                $(".file").show();
            });
            $("#pic_edit").click(function(){
                $("#save").show();
            });
            $("#pic_edit").click(function(){
                $("#pic_edit").hide();
            });
        });
</script>