
<div class="row">
	<div class="col-md-3 sections sidebar" style="">
		 <img src="<?php echo HTTP_ROOT?>img/profile/<?php echo $user['User']['image']; ?>" class="img-rounded" alt="Profile Picture" style="width: 49%; height: 0%; border: 1px solid; padding: 1%;"> 
		 <br> 
		 <div class="user_details"><?php echo $this->Session->read('Auth.User.name'); ?></div>
		 <div class="user_details"><?php echo $this->Session->read('Auth.User.email'); ?></div>
		 <div class="Options">
		 	<ul style="list-style-type:none;padding-left: 0%;">
		 		<li><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="<?php echo HTTP_ROOT?>Home/bookinghistory">Booked History</a></li>
		 		<li><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="<?php echo HTTP_ROOT?>Home/editprofile/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Edit Profile</a></li>
		 		<li><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="<?php echo HTTP_ROOT?>Home/addwallet/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Add to Wallet</a></li>
		 	</ul>
		 </div>
	</div>
	<div class="col-md-8 sections main-sec">
		<table width="60%">
			<tbody>
				<tr>
					<td><b>Name: </b></td>
					<td><?php echo $this->Session->read('Auth.User.name'); ?></td>
				
				
					<td><b>Email: </b></td>
					<td><?php echo $this->Session->read('Auth.User.email'); ?></td>
				</tr>
				<tr>
					<td><b>Gender: </b></td>
					<td><?php echo $this->Session->read('Auth.User.gender'); ?></td>
				
					<td><b>Mobile: </b></td>
					<td><?php echo $this->Session->read('Auth.User.phone'); ?></td>
				</tr>
			</tbody>
		</table>
		<br>
		<div class="wall">
			<i style="color:white;" class="fa fa-get-pocket" aria-hidden="true"></i> Wallet Balance: <?php echo $user['User']['wallet'];?>
		</div>
		<div class="booked">
			<div class="head"><h3>Booked History</h3></div>
			<table width="100%">
				<thead>
					<th>SNo</th>
					<!-- <th>Select</th> -->
					<th>Flight Number</th>
					<th>Source</th>
					<th>Destination</th>
					<th>Amount</th>
					<th>Passengers</th>
					<th>Status</th>
				</thead>
				<tbody>
					<?php $i=1; foreach($find as $data) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						
						<td><?php echo $data['Flight']['flight_no']; ?></td>
						<td><?php echo $data['From']['location']; ?></td>
						<td><?php echo $data['To']['location']; ?></td>
						<td><?php echo $data['Order']['amount']; ?></td>
						<td><?php echo $data['Order']['passengers']; ?></td>
						<td><?php if($data['Order']['status'] == 0)  echo "Failed"; else echo "Paid"; ?></td>
					</tr>
					<?php $i++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<style>
	.wall
	{
		background-color: #001743;
		color:white;
		padding: 2%;	
	}
	body
	{
		background-color: #E6F2FF;
	}
	.sections
	{
		background-color: white;
		margin-left: 2%;
	}
	.img-rounded {
    border-radius: 50%!important;
	}
	.sidebar
	{
		text-align: center;
		padding-top: 5%;
	}
	.user_details
	{
		text-align: center;
		margin-top: 4%;
	}
	.Options
	{
		text-align: left;
		margin-top: 21%; 
	}
	.Options > ul > li
	{
		padding: 3%;
	}
	.Options > ul > li:hover
	{
		background-color: #f2f2f2;
	}
	.fa
	{
		padding: 0%!important;
	}
	.main-sec
	{
		padding-top: 7%;
	}
	table,th,td
	{
		text-align: center;
	}
	.head
	{
		margin: 5% 0% 3% 2%;
	}
	.booked
	{
		margin-bottom: 3%;
	}
</style>
