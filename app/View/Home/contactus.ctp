<style>
	body
	{
		background-color: #E6F2FF;
	}
	.container
	{
		background-color: white;
	}
	.head
	{
		margin-left: 3%;
		margin-top: 3%;
	}
	.address
	{
		margin-top: 6%;
	}
	.space
	{
		margin-top: 1%; 
	}
</style>
<div class="container">
	<div class="head"><h2>Contact Us</h2></div>
	<div class="content">
		<div class="row">
			<div class="col-md-7"><img style="margin-top: 15%;" class="img-responsive" src="<?php echo HTTP_ROOT ?>img/index/contactus.jpg"></div>
			<div class="col-md-4">
				<h4 style="color: #138DF2;">Address</h4>
				<div class="address">
				<?php echo $data['address']['Content']['content'];?>
				<br><b>Fax:</b> <?php echo $data['fax']['Content']['content'];?>
				<br><b>Email:</b> <?php echo $data['email']['Content']['content'];?>

				<br><b>Enquiry:</b> <?php echo $data['enquiry']['Content']['content'];?></p></div>

				<div class="contact">
					<form action="" method="post" id="contactform">
						<h4 style="color: #138DF2; margin-bottom: 6%;">Contact US</h4>
						<input type="text" class="form-control space required" placeholder="Your Name" name="name">
						<input type="text" class="form-control space required email" placeholder="Your Email" name="email">
						<input type="text" class="form-control space required number" placeholder="Your Mobile Number" name="mobile" minlength="10" maxlength="10">
						<input type="text" class="form-control space required" placeholder="Your GST Number" name="gst">
						<textarea class="form-control space required" placeholder="Message" name="message"></textarea>
						<br><button class="btn btn-primary" style="width: 50%;">Send</button>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
<script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script>
<script type="text/javascript">
  $('#contactform').validate();
</script>