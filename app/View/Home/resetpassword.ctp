<style>
	body
	{
		background-color: #E6F2FF;
	}
	.content
	{
		background-color: white;
		padding-bottom: 2%;
	}
	.container 
	{
    	width: 850px!important;
	}
	.data
	{
		margin-top: 1%;
	}
</style>
<div class="container">
	<div class="content">
	<h3>Change Password</h3>
		<div class="row" style="margin-top: 5%;">
		<form action="" method="post" id="change-password">
			<div class="col-md-4 data">
				New Password: 
			</div>
			<div class="col-md-7 data">
				<input type="password" class="form-control" name="newpassword" id="newpassword">
			</div>
			<br>
			<div class="col-md-4 data">
				Confirm New Password: 
			</div>
			<div class="col-md-7 data">
				<input type="password" class="form-control" name="confirmnewpassword" id="confirmnewpassword">
			</div>
			<br>
			<div class="col-md-4 data">
				
			</div>
			<div class="col-md-7 data">
				<button type="submit" class="btn btn-primary" style="width: 35%;">Save</button>
			</div>
		</form>
		</div>
	</div>
</div>
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
  <script>
  $(document).ready(function () {
    
	baseUrl = 'http://'+$(location).attr('hostname')+'/Airfare/';




	    $('#change-password').validate({
	        rules: {
	                oldpassword: {
	                    required: true,
	                   remote: {
	                            url: baseUrl+"Home/check_password",
	                            type: "post",
	                            
	                         }
	                }        
	              },
	        messages : {
	                    oldpassword: {
	                        remote: "Please enter correct Current Password."
	                    }
	                
	                }     
	    });
	    $("#confirmnewpassword").rules('add',{equalTo: "#newpassword",
	    messages: {equalTo: "New password and confirm password field doesn't match."}});
	});
</script>