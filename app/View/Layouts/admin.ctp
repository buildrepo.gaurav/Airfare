<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<title>Admin</title>
		<link rel="shortcut icon" href="<?php //echo HTTP_ROOT.'img/bajrang-logo-favicon.png'?>" type="image/x-icon">

		<link rel="shortcut icon" href="<?php echo HTTP_ROOT ?>img/favicons/favicon.png">
		<link rel="icon" type="image/png" href="<?php echo HTTP_ROOT ?>img/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo HTTP_ROOT ?>img/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo HTTP_ROOT ?>img/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo HTTP_ROOT ?>img/favicons/favicon-160x160.png" sizes="160x160">
		<link rel="icon" type="image/png" href="<?php echo HTTP_ROOT ?>img/favicons/favicon-192x192.png" sizes="192x192">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HTTP_ROOT ?>img/favicons/apple-touch-icon-180x180.png">
		<!-- END Icons -->

		<!-- Stylesheets -->
		<!-- Web fonts -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

		<!-- Page JS Plugins CSS -->
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/slick/slick.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/slick/slick-theme.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/bootstrap.min.css">
		<link rel="stylesheet" id="css-main" href="<?php echo HTTP_ROOT ?>css/oneui.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/style.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/datatables/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/select2/select2.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/select2/select2-bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.min.js"></script>

		<?php //echo $this->Html->script(array('admin/jquery-1.8.2.js','admin/jquery.validate.js','admin/tooltip.js','admin/superfish.js','admin/common.js'))?>
  		<?php //echo $this->Html->css(array(''))?>
	</head>
	<body>
		<?php if(!in_array($this->params['action'],array('login','selectwarehouse'))){ ?>
		<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed"> 
		<?php echo $this->element('admin/dashboard_header'); } else{?>
		<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed sidebar-mini">
		<?php }?>
			<?php echo $content_for_layout?>
			<?php echo $this->element('admin/dashboard_footer'); ?>          
	  	</div> 
	  	<?php echo $this->element('admin/modal'); ?>

	  	<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js --> 
		 
		<script src="<?php echo HTTP_ROOT ?>js/core/bootstrap.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.slimscroll.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.scrollLock.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.appear.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.countTo.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/jquery.placeholder.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/core/js.cookie.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/app.js"></script> 
		<script src="https://use.fontawesome.com/f683610f82.js"></script>
		<!-- Page Plugins --> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/slick/slick.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/chartjs/Chart.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/datatables/jquery.dataTables.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/datatables/dataTables.bootstrap.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/select2/select2.full.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datetimepicker/moment.min.js"></script> 
		<script src="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script> 

		<!-- Page JS Code --> 
		 
		<script>
		    jQuery(function () {
		                // Init page helpers (Slick Slider plugin)
		        //App.initHelpers('slick');
		    });
		</script> 
		<script>
	<?php if($this->Session->check('success-msg')){?>
				$('#success-msg-show').text('<?php echo $this->Session->read('success-msg');?>');
				$('#msgmodal').modal('show');
				
		  <?php unset($_SESSION['success-msg']); }?>

		  <?php if($this->Session->check('pospopup')){?>
        $('#posmodal').modal('show');
        
      <?php unset($_SESSION['pospopup']); }?>

	</script>
		<script>

$(document).ready(function () {
	
	// $('.dataTables_filter').find('input').addClass('.form-control');

	/*$('.datepicker').datepicker({
	    format: 'yyyy-mm-dd',
	    startDate: '-3d'
	});*/

	

	$('.star').on('click', function () {
      $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function () {
      $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function () {
      var $target = $(this).data('target');
      if ($target != 'all') {
        $('.table-filter tr').css('display', 'none');
        $('.table-filter tr[data-status="' + $target + '"]').fadeIn('slow');
      } else {
        $('.table-filter tr').css('display', 'none').fadeIn('slow');
      }
    });

 });
 
</script> 
		<script>
		  $(function () {
		   
		    $('#userTable').DataTable({
		      "paging": true,
		      "lengthChange": true,
		      "searching": true,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false
		    });
		  });
		</script> 
	<script>
	$(document).ready(function()
	{
	    var navItems = $('.option-menu li > a');
	    var navListItems = $('.option-menu li');
	    var allWells = $('.option-content');
	    var allWellsExceptFirst = $('.option-content:not(:first)');
	    
	    allWellsExceptFirst.hide();
	    navItems.click(function(e)
	    {
	        e.preventDefault();
	        navListItems.removeClass('active');
	        $(this).closest('li').addClass('active');
	        
	        allWells.hide();
	        var target = $(this).attr('data-target-id');
	        $('#' + target).show();
	    });
	});
	</script>
	<script>
	$(document).ready(function() {
	    $("#add_row").on("click", function() {
	        // Dynamic Rows Code
	        
	        // Get max row id and set new id
	        var newid = 0;
	        $.each($("#tab_logic tr"), function() {
	            if (parseInt($(this).data("id")) > newid) {
	                newid = parseInt($(this).data("id"));
	            }
	        });
	        newid++;
	        
	        var tr = $("<tr></tr>", {
	            id: "addr"+newid,
	            "data-id": newid
	        });
	        
	        // loop through each td and create new elements with name of newid
	        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
	            var cur_td = $(this);
	            
	            var children = cur_td.children();
	            
	            // add new td and element if it has a nane
	            if ($(this).data("name") != undefined) {
	                var td = $("<td></td>", {
	                    "data-name": $(cur_td).data("name")
	                });
	                
	                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
	                c.attr("name", $(cur_td).data("name") + newid);
	                c.appendTo($(td));
	                td.appendTo($(tr));
	            } else {
	                var td = $("<td></td>", {
	                    'text': $('#tab_logic tr').length
	                }).appendTo($(tr));
	            }
	        });
	        
	        // add delete button and td
	        /*
	        $("<td></td>").append(
	            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
	                .click(function() {
	                    $(this).closest("tr").remove();
	                })
	        ).appendTo($(tr));
	        */
	        
	        // add the new row
	        $(tr).appendTo($('#tab_logic'));
	        
	        $(tr).find("td button.row-remove").on("click", function() {
	             $(this).closest("tr").remove();
	        });
	});




	    // Sortable Code
	    var fixHelperModified = function(e, tr) {
	        var $originals = tr.children();
	        var $helper = tr.clone();
	    
	        $helper.children().each(function(index) {
	            $(this).width($originals.eq(index).width())
	        });
	        
	        return $helper;
	    };
	  
	    $(".table-sortable tbody").sortable({
	        helper: fixHelperModified      
	    }).disableSelection();

	    $(".table-sortable thead").disableSelection();



	    $("#add_row").trigger("click");
	});
	</script> 
	<script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
            });
        </script>    
  </body>
</html>